--
-- store: NOTESDIRECT
--
-- property

insert into property (APN ,BUILT_YEAR ,City ,NO_OF_BATHROOMS ,NO_OF_BEDROOMS ,
    PROP_LOT_SIZE ,Property_Type , Building_size_sf,State ,Store_Name ,Store_PROPERTY_ID ,Street_Address ,Zip )
     select C12  as APN ,
     nullif(c35,'')  as BUILT_YEAR ,
     upper(C08) as City ,
     nullif(c32,'') as NO_OF_BATHROOMS ,
     nullif(c31,'') as NO_OF_BEDROOMS ,
     nullif(replace(replace(replace(upper(c34),'ACRES',''),'ACRE',''),' ',''),'') as PROP_LOT_SIZE ,
     CASE
       WHEN c05 = 'Single Family' then 'SFR'
         WHEN c05 = 'Modular' then 'SFR'
            WHEN c05 = 'Condominium' then 'CONDO'
             WHEN c05 = 'Multi Family' then  'MULTI-FAM'
              WHEN c05 = 'Townhouse' then  'TH'
               WHEN c05 = 'Manufactured Double-wide' then  'MFG-HOME'
                 WHEN c05 = 'Manufactured Single-wide' then  'MFG-HOME' ELSE 'OTHER'
      END AS Property_type ,
               nullif(c33,'')  as Building_size_sf ,
               C09 as State ,
               '$STORE_NAME' as store_name ,
               c01 as Store_PROPERTY_ID ,
               concat(c06,c07) as Street_Address ,
               C10  as Zip
                 from load_temp
                  where not exists (select 1 from property where Store_Name='$STORE_NAME'
                   and Store_PROPERTY_ID=c01);


-- note

insert into note (ESTIMATED_MARKET_VALUE,user_id,Interest_rate_Initial,loan_type, Note_Position,NOTE_PRICE,Note_Type,
  ORIGINAL_LOAN_BAL,Origination_Date,PDI_PAYMENT,Property_id,
  REMAINING_NO_OF_PAYMENT,Store_Name,Store_Note_id,Term_Months,UNPAID_BALANCE )
 select
   t.c29 VALUE,
   $USER,
   t.c23,
   'CUST_FIXED',
   t.c26,
   replace(replace(replace(t.c36,'$',''),',',''),' ',''),
   substr(t.c04,1,1),
   t.c19,
   to_date(nullif(t.C13,'0000-00-00'),''),
   t.c21,
   p.Property_id,
   t.c25,
   '$STORE_NAME',
   t.c01,
   TIMESTAMPDIFF(MONTH, to_date(nullif(t.C15,'0000-00-00'),''), to_date(nullif(t.C14,'0000-00-00'),''))+1,
   t.c20
  from load_temp t, property p
  where store_name='$STORE_NAME' and p.Store_PROPERTY_ID = t.c01;


-- property_areas

insert ignore into property_areas (area_id, area_type,property_id)
 select a.area_id,a.area_type ,p.property_id
  from area a, property p,load_temp t
  where area_type='CITY'
    and p.store_name='$STORE_NAME'
    and p.Street_Address=concat(t.c06,t.c07)
    and Upper(t.C08)=p.City
    and upper(t.C09)=p.State
    and t.C10=p.ZIP
    and upper(concat(p.state,'-',p.city))=a.area_id
     on duplicate key update property_areas.Property_id=property_areas.Property_id;

insert ignore into property_areas (area_id, area_type,property_id)
 select a.area_id,a.area_type ,p.property_id
 from area a, property p,load_temp t
 where area_type='ZIP-CITY-STATE'
   and p.store_name='$STORE_NAME'
   and p.Street_Address=concat(t.c06,t.c07)
   and Upper(t.C08)=p.City
   and upper(t.C09)=p.State
   and t.C10=p.ZIP
   and upper(concat(p.ZIP,'-',p.city,'-',p.state))=a.area_id
   on duplicate key update property_areas.Property_id=property_areas.Property_id;


--
-- store: LOCAL
--
-- property

insert ignore into property (Property_Type,Street_Address,City,State,Zip,Built_Year,
                              Prop_Lot_Size,Building_size_sf,No_of_bathrooms,No_of_bedrooms,APN,
                              Store_name,Store_Property_id)
 select
  C01,
  C02,
  upper(c03),
  upper(C04),
  C05,
  nullif(c16,'')  as BUILT_YEAR,
  nullif(replace(replace(replace(upper(c17),'ACRES',''),'ACRE',''),' ',''),'') as PROP_LOT_SIZE,
  nullif(c18,'') as Building_size_sf,
  nullif(c19,'') as NO_OF_BATHROOMS,
  nullif(c20,'') as NO_OF_BEDROOMS ,
  c21 as APN,
  '$STORE_NAME' as store_name,
  null as Store_PROPERTY_ID
  from load_temp
   where c01 is not null and c01<>''
   and not exists (select 1
                    from property
                     where Store_Name='$STORE_NAME' and Street_Address=c02
                                                    and Upper(C03)=City
                                                     and upper(C04)=State
                                                      and C05=ZIP);


-- note

insert ignore into note (ESTIMATED_MARKET_VALUE,Note_Type,Origination_Date,
                          REMAINING_NO_OF_PAYMENT,last_payment_recieved_date,ORIGINAL_LOAN_BAL,
                          Interest_rate_Initial,Loan_Type,Note_Position,Borrower_Credit_Score,
                          Note_Price,Store_Name,user_id,Property_id,term_months,Unpaid_balance)
 select
    nullif(c26,'') as ESTIMATED_MARKET_VALUE,
    t.c06 as Note_Type,
    str_to_date(nullif(t.C07,''),'%m/%d/%Y') as Origination_Date,
    nullif(c22,'') as Remaining_Payments,
    str_to_date(nullif(t.C23,''),'%m/%d/%Y') as last_payment_recieved_date,
    replace(replace(replace(t.c08,'$',''),',',''),' ','') as ORIGINAL_LOAN_BAL,
    replace(replace(replace(t.c09,'%',''),',',''),' ','') Interest_Rate,
    nullif(c13,'') Loan_Type,
     nullif(c10,'') Note_Position,
    nullif(c14,'') Borrower_Credit_Score,
    replace(replace(replace(t.c11,'$',''),',',''),' ','') Note_Price,
    '$STORE_NAME',
    $USER,
    p.Property_id,
    nullif(c12,''),
    nullif(c15,'')

     from load_temp t, property p
      where  t.c02<>''
      and p.store_name='$STORE_NAME'
      and p.Street_Address=t.c02
      and Upper(t.C03)=p.City
      and upper(t.C04)=p.State
      and t.C05=p.ZIP;


-- property_areas


insert ignore into property_areas (area_id, area_type,property_id)
 select a.area_id,a.area_type ,p.property_id
 from area a, property p,load_temp t
 where area_type='CITY'
   and p.store_name='$STORE_NAME'
   and p.Street_Address=t.c02
   and Upper(t.C03)=p.City
   and upper(t.C04)=p.State
   and t.C05=p.ZIP
   and upper(concat(p.state,'-',p.city))=a.area_id
               on duplicate key update property_areas.Property_id=property_areas.Property_id;

insert ignore into property_areas (area_id, area_type,property_id)
 select a.area_id,a.area_type ,p.property_id
  from area a, property p,load_temp t
   where area_type='ZIP-CITY-STATE'
    and p.store_name='$STORE_NAME'
    and p.Street_Address=t.c02
    and Upper(t.C03)=p.City
    and upper(t.C04)=p.State
    and t.C05=p.ZIP
    and upper(concat(p.ZIP,'-',p.city,'-',p.state))=a.area_id
              on duplicate key update property_areas.Property_id=property_areas.Property_id;

