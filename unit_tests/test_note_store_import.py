#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import note_store_import

class TestStoreImport(unittest.TestCase):

    def setUp(self):
        self.get_message = note_store_import.get_message('OperationalError')

    def tearDown(self):
        pass

    def test_(self):
        self.assertEqual(self.get_message, 'Unknown Date Format')



if __name__ == '__main__':
    unittest.main()