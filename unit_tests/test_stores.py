#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest

from stores.local_store import Store as local_store
from stores.notesdirect_store import Store as notesdirect_store

class TestLocalStoreImport(unittest.TestCase):

    def setUp(self):
        self.store = local_store()

    def tearDown(self):
        pass

    def test_query1(self):
        response = self.store.query1()
        self.assertEqual(response, 4)


class TestNotesdirectStoreImport(unittest.TestCase):
    def setUp(self):
        self.store = notesdirect_store()

    def tearDown(self):
        pass

    def test_query1(self):
        response = self.store.query1()
        self.assertEqual(response, 4)



if __name__ == '__main__':
    unittest.main()