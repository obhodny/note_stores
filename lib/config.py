DIR ='/opt/tomcat/uploadedFile/'
DB = 'nt'
TMP = "/tmp/"
TMP1 = "/tmp/"
SLEEP = 1
MASK = '^[0-9][0-9]*__.*.csv'
ARCHIVE = "/opt/tomcat/uploadedFile/"
HOST = 'localhost'
USER ='notes'
PASSWORD ='notes'
CHARSET = 'utf8'
# LOG_FORMAT = '%(levelname)s:  %(message)s'
LOG_FORMAT = '%(message)s'
STORES_DIR = 'stores.'
REPLACE_CHARACTER_LIST = ['(standard input):']
greatschools_api_key = 'b2ce8e7f510aee1cd483a7da21c7a9ec'
SCHOOL_RANGE = '12'
PRIVILRGE_CODES = ['NOTE_CREATE_OR_UPLOAD']

# Errors

ERROR_NOTE_CREATE_OR_UPLOAD = 1
ERROR_NOTE_CREATE_OR_UPLOAD_MESSAGE = 'your subscription does not provide for uploading notes'
ERROR_BAD_HEADER = 2
ERROR_BAD_HEADER_MESSAGE = 'We could not detect your CSV file note store format. ' \
                         'Please make sure first line of the CSV contain column names.'


