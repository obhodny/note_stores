import json
import urllib
import urllib2
from urlparse import urljoin

class Http(object):
  def __init__(self, url, api_key=None, debug=False):
    self.api_key = api_key
    self.url = url
    self._debug = debug

  def _call_server_post(self, url, post_data):
    if not url:
      response = dict(error=True, message='AttributeError: not URL')
      return json.dumps(response)
    data = urllib.urlencode(post_data)
    request = urllib2.Request(url, data)
    try:
      res = urllib2.urlopen(request)
    except urllib2.HTTPError as e:
      raise
    except urllib2.URLError as e:
      raise
    else:
      response = res.read()
    return response

  def _call_server_get(self, url):
    try:
      res = urllib2.urlopen(url)
    except urllib2.HTTPError as e:
      raise
    except urllib2.URLError as e:
      raise
    else:
      response = res.read()
    return response

  def post(self, url, data):
    response = self._call_server_post(url, data)
    return response

  def get(self, url):
    response = self._call_server_get(url)
    return response

  def get_api(self, api_name, state, zip=None, id=None):
    if not id:
      url = '{url}/{api_name}?key={api_key}&state={state}&zip={zip}'.format(
        url=self.url, api_name=api_name, api_key=self.api_key, state=state, zip=zip)
    elif id:
      url = '{url}/{api_name}/{state}/{id}?key={api_key}'.format(
        url=self.url, api_name=api_name, api_key=self.api_key, state=state, id=id)
    else:
      url = ''
    print url
    response = self.get(url)
    return response