FIELD_MAP = {
  '...':'Property ID',
  '...':'Servicer Loan ID',
  '...':'Servicer Name',
  '...':'Asset Type',
  '...':'Property Type',
  '...':'Property Address 1',
  '...':'Property Address 2',
  '...':'Property City',
  '...':'Property State',
  '...':'Property Zip',
  '...':'Property County',
  '...':'Property APN/TaxID',
  'Date_for_calculation':'Origination Date',
  'Maturity_Date': 'Maturity Date',
  '...':'First Payment Due',
  '...':'Next Payment Due',
  '...':'Last Payment Received',
  '...':'Loan Status',
  '...':'Orig Loan Amount',
  '...':'Current UPB',
  '...':'P+I Payment',
  '...':'Escrow Payment',
  '...':'Interest Rate',
  '...':'Term',
  '...':'Est. Pmts Remaining',
  '...':'Lien Position',
  '...':'Valuation Type',
  '...':'Valuation Date',
  '...':'Valuation Amount',
  '...':'# of rooms',
  '...':'# of bedrooms',
  '...':'# of bathrooms',
  '...':'Sq. Ft.',
  '...':'Lot Size',
  '...':'Year Built',
  '...':'Pricing: Retail',
  '...':'Pricing: Titanium',
  '...':'Pricing: Diamond',
  '...':'Pricing: Platinum (BM)',
  '...':'Pricing: Platinum (RR)'
}

def replace_field_name(message):
  try:
    for key, value in FIELD_MAP.items():
      message = message.replace(key, value)
  except:
    return message
  return message

def to_utf8(text):
  if isinstance(text, unicode):
    text = text.encode('utf-8')
  return text

def prepare_mysql_error(error):
  message = ''
  try:
    if type(error).__module__ != '_mysql_exceptions':
      return str(error)

    if len(error.args) == 0:
      return str(error)

    if len(error.args) == 1:
      message = str(error.args[0])
    elif len(error.args) > 1:
      message = str(error.args[1])

    message = message.replace('at row 1', '').strip()

    return message if message else str(error)
  except:
    return error
