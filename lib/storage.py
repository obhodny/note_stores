try:
  import MySQLdb as mysql
except ImportError:
  pass
import config as config
import logging
import time
import utils

def db_connect():
  connect = mysql.connect(
    host=config.HOST, db=config.DB,
    user=config.USER, passwd=config.PASSWORD,
    charset=config.CHARSET
  )
  return connect

class AS_IS(object):
  def __init__(self, value):
    self.value = value

class IS_NULL(object):
  pass

class IS_NOT_NULL(object):
  pass

class BaseStorage(object):
  AS_IS = AS_IS
  IS_NULL = IS_NULL
  IS_NOT_NULL = IS_NOT_NULL
  TABLE_NAME = None
  ID_FIELD = 'id'
  PARAM_CHAR = '%s'
  TRUE = 1
  FALSE = 0
  LoggerName = 'request'

  def __init__(self, table=None, id_field=None, connect=None):
    self._connect = None
    self._cursor  = None
#    self._execute = None
    self.reconnect(connect)
    if table is None:
      table = self.TABLE_NAME
    if id_field is None:
      id_field = self.ID_FIELD
    self._table = table
    self._id_field = id_field
    self._debug = True
    self.log = logging.getLogger(self.LoggerName)

  id_field = property(lambda self: self._id_field)

  def _close_cursor(self):
#    self._execute = None
    self._cursor.close()
    self._cursor = None

  def _execute(self, *args, **kwargs):
    try:
      self._cursor.execute(*args, **kwargs)
    except Exception, error:
      str_error = repr(error)
      if (('InterfaceError' in str_error) or ('OperationalError' in str_error)):
        self.reconnect()
        self._cursor.execute(*args, **kwargs)
      else:
        raise error

  def reconnect(self, connect=None):
    if connect is None:
      connect = db_connect()
    self._connect = connect
    self._cursor  = self._connect.cursor()
#    self._execute = self._cursor.execute

  def _prepare_where(self, where, where_values=None):
    where_array = []
    if where_values is None:
      where_values = []
    if where:
      if   isinstance(where, dict):
        where_array = []
        for name, value in where.items():
          if   isinstance(value, self.IS_NULL):
            w1 = "%s IS NULL" % name
          elif isinstance(value, self.IS_NOT_NULL):
            w1 = "%s IS NOT NULL" % name
          else:
            w1 = "%s = %s" % (name, self.PARAM_CHAR)
            where_values.append(value)
          where_array.append(w1)
        where = " AND ".join(where_array)
      elif not isinstance(where, basestring):
        where = None
    else:
      where = None
    return (where, where_values)

  def _prepare_dict_items(self, data, cursor):
    descriptions = [ desc[0] for desc in cursor.description ]
    res = []
    for item in data:
      item2 = dict([
        (name, value) for name, value in map(None, descriptions, item)
      ])
      res.append(item2)
    return res

  def _prepare_dict_item(self, data, cursor):
    if data:
      res = dict([
        (desc[0], value) for desc, value in map(None, cursor.description, data)
      ])
    else:
      res = None
    return res

  def _prepare_limit_offset(self, limit=None, offset=None):
    if limit is not None:
      if offset is not None:
        text_limit = 'LIMIT %s, %s' % (offset, limit)
      else:
        text_limit = 'LIMIT %s' % limit
    else:
      text_limit = None
    return text_limit

  def close(self):
    self._close_cursor()
    self._connect.close()
    self._connect = None

  def with_transaction(self, function, *args, **kwargs):
    self.begin()
    error = None
    try:
      res = function(*args, **kwargs)
    except Exception, error:
      if self._debug:
        print error
    if error:
      self.rollback()
      raise error
    else:
      self.commit()
    return res

  def begin(self):
    self._execute('BEGIN')

  def commit(self):
    self._execute('COMMIT')

  def rollback(self):
    self._execute('ROLLBACK')

  def delete(self, where=None, table=None, debug=False):
    table = table if table else self._table
    command = "DELETE FROM %s" % table
    where, where_values = self._prepare_where(where)
    if where:
      command += " WHERE %s" % where
    if debug:
      print command
      print 'values:', where_values
    self._execute(command, where_values)

  def insert(self, data, table=None, debug=False):
    table = table if table else self._table
    names = data.keys(); values = data.values()
    names = ", ".join(names)
    holders = ", ".join([self.PARAM_CHAR]*len(values))
    command = 'INSERT INTO %s (%s) VALUES (%s)' % (
      table, names, holders
    )
    if debug:
      print command, values
    self._execute(command, tuple(values))
    return self._cursor.lastrowid

  def insert_with_ignore(self, data, table=None, debug=False):
    table = table if table else self._table
    names = data.keys(); values = data.values()
    names = ", ".join(names)
    holders = ", ".join([self.PARAM_CHAR]*len(values))
    command = 'INSERT IGNORE INTO %s (%s) VALUES (%s)' % (
      table, names, holders
    )
    if debug:
      print command, values
    self._execute(command, tuple(values))
    return self._cursor.lastrowid

  def update(self, data, where, table=None, debug=False):
    table = table if table else self._table
    sets_array = []; values = []
    for name, value in data.items():
      if isinstance(value, self.AS_IS):
        sets_array.append("%s = %s" % (name, value.value))
      else:
        sets_array.append("%s = %s" % (name, self.PARAM_CHAR))
        values.append(value)
    sets = ", ".join(sets_array)
    command = "UPDATE %s SET %s" % (table, sets)
    if where:
      where, where_values = self._prepare_where(where)
      command += " WHERE %s" % where
      values.extend(where_values)
    if debug:
      print command, values
    self._execute(command, values)

  def get_by_id(self, id, select=None, as_dict=False, debug=False):
    return self.get({ self._id_field: id }, select, as_dict=as_dict, debug=debug)

  def update_by_id(self, id, data, debug=False):
    self.update(data, { self._id_field: id }, debug=debug)

  def delete_by_id(self, id, debug=False):
    self.delete({ self._id_field: id }, debug=debug)

  def get(self, where, select=None, order_by=None, as_dict=False, table=None,
    where_values=None, debug=False
  ):
    if select is None:
      select = '*'
    table = table if table else self._table
    command = "SELECT %s FROM %s" % (select, table)
    where, where_values = self._prepare_where(where, where_values)
    if where:
      command += " WHERE %s" % where
    if order_by:
      command += " ORDER BY %s" % order_by
    command += " LIMIT 1"
    if debug:
      print command
      print 'values:', where_values
    self._execute(command, where_values)
    res = self._cursor.fetchone()
    if res and as_dict:
      res = self._prepare_dict_item(res, self._cursor)
    return res

  def getall(self, where=None, select=None, group_by=None, order_by=None, limit=None, join=None,
    distinct=False, as_dict=False, debug=False, text_limit=None
  ):
    if select is None:
      select = '*'
    distinct = 'DISTINCT' if distinct else ''
    command = "SELECT %s %s FROM %s" % (distinct, select, self._table)
    where, where_values = self._prepare_where(where)
    if join:
      if isinstance(join, str):
        join = 'JOIN %s' % join
      else:
        join_array = []
        for item in join:
          if isinstance(join, str):
            item = 'JOIN %s' % item
          else:
            item = 'JOIN %s ON %s' % tuple(item)
          join_array.append(item)
        join = '\n'.join(join_array)
      command += '\n %s' % join
    if where:
      command += " WHERE %s" % where
    if group_by:
      command += " GROUP BY %s" % group_by
    if order_by:
      command += " ORDER BY %s" % order_by
    if limit:
      command += " LIMIT %s" % limit
    if text_limit:
      command += '\n' + text_limit
    if debug:
      print command
      print 'values:', where_values
#    log.error('sql: %s', command)
    self._execute(command, where_values)
    res = self._cursor.fetchall()
    if res and as_dict:
      res = self._prepare_dict_items(res, self._cursor)
    return res

  def get_all(self, *args, **kwargs):
    return self.getall(*args, **kwargs)

  def get_list(self, where, field=None, debug=False, **kwargs):
    if field is None:
      field = self._id_field
    data = self.getall(where, field, debug=debug, **kwargs)
    data = [ item[0] for item in data ]
    return data

  def exists(self, where, field=None, debug=False):
    field = field if field else self._id_field
    res = self.get(where, field, debug=debug)
    if res is not None:
      res = res[0]
    return res

  def clear_table(self):
#    self.delete()
    self.with_transaction(self.delete)

  def _write_data(self, data):
    insert = self.insert
    for item in data:
      insert(item)

  def write_data(self, data):
    self.with_transaction(self._write_data, data)

  def update_data(self, data):
    return self.with_transaction(self._update_data, data)

  def execute(self, command, values=None):
    if values is not None:
      self._execute(command, values)
    else:
      self._execute(command)
    return self._cursor

  def execute_and_fetch(self, command, values=None, as_dict=False, debug=False):
    if debug:
      print 'command:\n', command
      print 'values:', repr(values)
    cursor = self.execute(command, values)
    res = cursor.fetchall()
#    if res and as_dict:
#      res = self._prepare_dict_items(res, cursor)
    if res:
      if as_dict:
        res = self._prepare_dict_items(res, cursor)
      else:
        res = list(res)
    else:
      res = []
    return res

  def add(self, __data=None, debug=False, table=None, **kwargs):
    data = __data and __data or kwargs
    return self.with_transaction(self.insert, data, table=table, debug=debug)

  def add_with_ignore(self, __data=None, debug=False, **kwargs):
    data = __data and __data or kwargs
    return self.with_transaction(self.insert_with_ignore, data, debug=debug)

  def save(self, id, __data=None, debug=False, **kwargs):
    data = __data and __data or kwargs
#    print 'save:', repr(id), repr(data)
    return self.with_transaction(self.update_by_id, id, data, debug=debug)

  def insert_by_command(self, command, table=None, debug=False):
    table = table if table else self._table
    if debug:
      print command
    self._execute(command)
    return self._cursor.lastrowid

class NoteStorage(BaseStorage):
  TABLE_NAME = 'note'
  ID_FIELD = 'Note_id'

  def add_or_update(self, data, user_id, address, city, state, zip, debug=False):
    where ='user_Id = {user_id} ' \
           ' AND Street_Address = "{address}" ' \
           ' AND City = "{city}" ' \
           ' AND  Zip = "{zip}" ' \
           ' AND State = "{state}" '.format(user_id=user_id,
                                             address=address,
                                             city=city,
                                             state=state,
                                             zip=zip)
    property = self.get(where=where, table='property', as_dict=True, debug=debug)
    if not property:
      id = self.add_with_ignore(data)
    else:
      note = self.get_by_property(user_id, property['Property_id'])
      if note:
        data.update({'UPDATED_DATE_TIME' : time.strftime('%Y-%m-%d %H:%M:%S')})
        self.save(note['Note_id'], data)
        id = note['Note_id']
      else:
        id = self.add_with_ignore(data)
    return id

  def get_by_property(self, user_id, property_id, debug=None):
    where = 'User_Id = {0} AND Property_id = {1}'.format(user_id, property_id)
    return self.get(where=where, as_dict=True, debug=debug)


  def get_notes_by_user(self, user_id):
    command ="""
     SELECT *
     FROM note
     WHERE user_id = {user_id}  OR parent_note_id IN (SELECT note_id FROM note WHERE user_id = {user_id} );
    """.format(user_id=user_id)
    return self.execute_and_fetch(command, as_dict=True)

  def get_count_user_notes(self, user_id):
    command ="""
     SELECT COUNT(Note_id) as number
     FROM note
     WHERE user_id = {user_id}  OR parent_note_id IN (SELECT note_id FROM note WHERE user_id = {user_id} );
    """.format(user_id=user_id)
    data = self.execute_and_fetch(command, as_dict=True)
    res = data[0]['number'] if data else None
    return res

class NoteStoreStorage(BaseStorage):
  TABLE_NAME = 'note_store'

class InterfaceTableMappingStorage(BaseStorage):
  TABLE_NAME = 'interface_table_mapping'

class TempStorage(BaseStorage):
  TABLE_NAME = 'load_temp'

class ApplicationCodesStorage(BaseStorage):
  TABLE_NAME = 'application_codes'

  def get_application_code(self, error):
    error = str(error).replace("'", "`")
    try:
      command = """
         CALL get_application_code('{error}', 'note_store_import', @msg, @sev);
          """.format(error=error)
      self.execute(command)
    except mysql.Error as e:
      return utils.prepare_mysql_error(e)
    command2 = """ SELECT @msg; """
    data = self.execute_and_fetch(command2, as_dict=True)
    if data:
      return data[0]['@msg']


class PropertyStorage(BaseStorage):
  TABLE_NAME = 'property'
  ID_FIELD = 'Property_id'

  def add_if_not_exist(self, data, user_id, address, city, state, zip, debug=False):
    where ='user_Id = {user_id} ' \
           ' AND Street_Address = "{address}" ' \
           ' AND City = "{city}" ' \
           ' AND  Zip = "{zip}" ' \
           ' AND State = "{state}" '.format(user_id=user_id,
                                           address=address,
                                           city=city,
                                           state=state,
                                           zip=zip)
    res = self.get(where=where, as_dict=True, debug=debug)
    if not res:
      property_id = self.add_with_ignore(data)
    else:
      property_id = res.get('Property_id')
    return property_id

  def add_or_update(self, data, user_id, address, city, state, zip, debug=False):
    where ='user_Id = {user_id} ' \
           ' AND Street_Address = "{address}" ' \
           ' AND City = "{city}" ' \
           ' AND  Zip = "{zip}" ' \
           ' AND State = "{state}" '.format(user_id=user_id,
                                             address=address,
                                             city=city,
                                             state=state,
                                             zip=zip)
    res = self.get(where=where, as_dict=True, debug=debug)
    if not res:
      property_id = self.add_with_ignore(data)
    else:
      data.pop('Street_Address')
      data.pop('City')
      data.pop('State')
      data.pop('Zip')
      property_id = res.get('Property_id')
      try:
        self.save(property_id, data)
      except Exception as e:
        print str(e)
    return property_id

class DataIntegritytorage(BaseStorage):
  TABLE_NAME = 'data_integrity'

class ParmeterStorage(BaseStorage):
  TABLE_NAME = 'parameters'

  def get_upload_location(self):
    res = self.getall(where="parameter_name = 'FILE_UPLOAD_LOCATION'", as_dict=True)
    if res:
      return res[0]['parameter_value']

class PropertyAreasStorage(BaseStorage):
  TABLE_NAME = 'property_areas'

  def get_by_property_id(self, property_id, debug=False):
    return self.get(where="Property_id = {0}".format(property_id), as_dict=True, debug=debug)

class AreaStorage(BaseStorage):
  TABLE_NAME = 'area'

  def get_by_property_id_type(self, area_id, area_type, debug=False):
    return self.get(where="Area_Id = '{0}' and Area_type = '{1}'".format(area_id, area_type), as_dict=True, debug=debug)

class SchoolsStorage(BaseStorage):
  TABLE_NAME = 'schools'

  def get_by_ids(self, ids):
    str_of_id = ','.join([str(x) for x in ids])
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '12'"
    where += ("\nAND id in ({0})".format(str_of_id))
    select ="zip, state, city, (sum(gsRating) / count(id)) as gs_rating"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

  def get_elementary(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '4'"
    select ="zip, state, city, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

  def get_middle(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '8'"
    select ="zip, state, city, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

  def get_high(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '12'"
    select ="zip, state, city, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

class Schools2Storage(BaseStorage):
  TABLE_NAME = 'schools2'

  def get_elementary(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '4'"
    select = "zip, state, city, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

  def get_middle(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '8'"
    select = "zip, state, city, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

  def get_high(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '12'"
    select = "zip, state, city, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'zip, state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)
  """
  select zip,city,state, sum(gsrating)/count(id) 
   from schools2
   where substring_index(graderange,'-',-1)=12 
   group by zip,city,state;  
 
  """

  def get_high_by_city(self):
    where = "SUBSTRING_INDEX(gradeRange, '-', -1) = '12'"
    select = "city, state, (sum(gsRating) / count(id)) as gs_rating, gradeRange"
    group_by = 'state, city'
    return self.getall(where=where, select=select, group_by=group_by, as_dict=True)

class EvictionStorage(BaseStorage):
  TABLE_NAME = 'eviction'

#TODO - get 'eviction_rate' as float
  def update_data(self):
    command="""
SELECT id, year, name as sity, parent_location as state, CONCAT(name,'-',parent_location) as area,
        ROUND(eviction_rate , 2) as eviction_origin,
        ROUND(eviction_rate * (if(@val=name, @i:=@i+0.2, @i:=0.2+least(0, @val:=name))), 1) as eviction_rate,
        ROUND(@i, 1) as weight
FROM eviction,  (select @i:=0.2, @val:='') dev_nul
WHERE year in('2016', '2015', '2014', '2013', '2012')
GROUP BY year, name, parent_location
ORDER BY year asc;
    """
    data = self.execute_and_fetch(command, as_dict=True)
    for item in data:
      in_data = dict(eviction_weight=item.get('weight'),
                     eviction_rate_with_weight=item.get('eviction_rate'))
      self.update_by_id(int(item.get('id')), in_data)
      # print item.get('id'), ' - ', in_data

  def get_data(self):
    command ="""
SELECT
  name as sity,
  parent_location as state,
  ROUND(AVG(eviction_rate_with_weight) , 2) as eviction_rate
FROM eviction
WHERE year in('2016', '2015', '2014', '2013', '2012')
GROUP BY  name, parent_location;
    """
    return self.execute_and_fetch(command, as_dict=True)

  def get_area(self, area_id):
    command = """
SELECT
  area.Area_Id, area_relations.Parent_Area_Id, area_relations.Area_Id as result
FROM area
LEFT JOIN area_relations ON area.Area_Id = area_relations.Parent_Area_Id
WHERE area.Area_type = "CITY" AND area.Area_Id = "{0}";
    """.format(area_id)
    return self.execute_and_fetch(command, as_dict=True)

class StatisticsStorage(BaseStorage):
  TABLE_NAME = 'statistics'

class ZipCodesStorage(BaseStorage):
  TABLE_NAME = 'zipcodes'

class UserSubscriptionsStorage(BaseStorage):
  TABLE_NAME = 'user_subscriptions'

  def get_by_user_id(self, user_id):
    command = """
      SELECT *
      FROM user_subscriptions
      WHERE user_id ={user_id} AND  is_active=1
      ORDER BY user_subscriptions.UA_Signed_on desc, user_subscriptions.USER_SUBSCRIPTION_ID desc
      LIMIT 1
        """.format(user_id=user_id)
    return self.execute_and_fetch(command, as_dict=True)

  def get_default(self, user_id):
    command = """
     select *
     from user_subscriptions u
      where u.user_id ={user_id}
       and u.subscription_name IN (select s.subscription_name from subscription s where s.type='default')
        ORDER BY u.UA_Signed_on desc, u.USER_SUBSCRIPTION_ID desc
        """.format(user_id=user_id)
    return self.execute_and_fetch(command, as_dict=True)

class SubscriptionPrivilegesStorage(BaseStorage):
  TABLE_NAME = 'subscription_privileges'

  def get_by_name(self, subscription_name, privilege_code=None):
    where = ' AND subscription_privileges.privilege_code = "{0}"'.format(privilege_code) if privilege_code else ''
    command = """
  SELECT *
  FROM subscription_privileges
  WHERE subscription_privileges.subscription_name = "{subscription_name}"
    {where}
    """.format(subscription_name=subscription_name, where=where)
    return self.execute_and_fetch(command, as_dict=True)

class DBManager(object):

  # def __init__(self, connect=None):
  #   self._connect = connect
  #
  # def __call__(self, connect=None):
  #   connect = connect if connect is not None else self.create_connect()
  #   return DBManager(connect)
  #
  # def create_connect(self):
  #   return db_connect()

  # connect = property(lambda self: self._connect)

  note = property(lambda self: NoteStorage())
  note_store = property(lambda self: NoteStoreStorage())
  interface_table_mapping = property(lambda self: InterfaceTableMappingStorage())
  temp = property(lambda self: TempStorage())
  base_storage = property(lambda self: BaseStorage())
  application_codes = property(lambda self: ApplicationCodesStorage())
  property_db = property(lambda self: PropertyStorage())
  property_areas = property(lambda self: PropertyAreasStorage())
  area = property(lambda self: AreaStorage())
  parameters = property(lambda self: ParmeterStorage())
  data_integrity = property(lambda self: DataIntegritytorage())
  schools = property(lambda self: SchoolsStorage())
  eviction = property(lambda self: EvictionStorage())
  statistics = property(lambda self: StatisticsStorage())
  zipcodes = property(lambda self: ZipCodesStorage())
  schools2 = property(lambda self: Schools2Storage())
  user_subscriptions = property(lambda self: UserSubscriptionsStorage())
  subscription_privileges = property(lambda self: SubscriptionPrivilegesStorage())

db = DBManager()