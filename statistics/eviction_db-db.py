#!/usr/bin/python
#-*- coding':' utf-8 -*-
#
# Author':' Vladimir <obhodny@gmail.com>
# Created':' 2018-01-23
#

"""eviction_db-db.py
Updating nt.statistics
add new statistic "EViCTION"
get from nt.eviction, prepare and insert to nt.statistic

"""

import os
from lib.storage import db, mysql
import warnings
import logging, logging.handlers
import lib.config as config
import datetime
from lib.utils import to_utf8

base_path = os.path.dirname(os.path.abspath(__file__))
formatter = logging.Formatter(config.LOG_FORMAT)
logging.root.setLevel(logging.NOTSET)

class MyLogger:
  def __init__(self, file_name):
    path_list = base_path.split('/')
    path_list.pop(len(path_list) - 1)
    log_path = '/'.join(path_list)
    self.log_file = '{log_path}/logs/{file_name}.log'.format(log_path=log_path, file_name=file_name)
    self.hdlr = logging.FileHandler(self.log_file, 'a')
    self.hdlr.setFormatter(formatter)
    self.logger = logging.getLogger(self.log_file)
    self.logger.addHandler(self.hdlr)

logger = MyLogger('eviction_rate')
log = logger.logger

state_map = {
'Alaska':'AK',
'Alabama':'AL',
'Arkansas':'AR',
'Arizona':'AZ',
'California':'CA',
'Colorado':'CO',
'Connecticut':'CT',
'Delaware':'DE',
'Florida':'FL',
'Georgia':'GA',
'Hawaii':'HI',
'Iowa':'IA',
'Idaho':'ID',
'Illinois':'IL',
'Indiana':'IN',
'Kansas':'KS',
'Kentucky':'KY',
'Louisiana':'LA',
'Massachusetts':'MA',
'Maryland':'MD',
'Maine':'ME',
'Michigan':'MI',
'Minnesota':'MN',
'Missouri':'MO',
'Mississippi':'MS',
'Montana':'MT',
'North Carolina':'NC',
'North Dakota':'ND',
'Nebraska':'ND',
'New Hampshire':'NH',
'New Jersey':'NJ',
'New Mexico':'NM',
'Nevada':'NV',
'New York':'NY',
'Ohio':'OH',
'Oklahoma':'OK',
'Oregon':'OR',
'Pennsylvania':'PA',
'Rhode Island':'RI',
'South Carolina':'SC',
'South Dakota':'SD',
'Tennessee':'TN',
'Texas':'TX',
'Utah':'UT',
'Virginia':'VA',
'Vermont':'VT',
'Washington':'WA',
'Wisconsin':'WI',
'West Virginia':'WV',
'Wyoming':'WY',
'District of Columbia':'DC'
}

def get_data():
  return db.eviction.get_data()

def get_area_id(area_id):
  return db.eviction.get_area(area_id)

def insert_db(table, data):
  response = dict(error=False, error_type='', message='', id=None)
  with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
      id = db.base_storage.add(data, table=table, debug=False)
      response.update(id=id)
    except mysql.Error as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    except Warning as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    finally:
      pass
  if response['error']:
    _write_log('error', response['error_type'], response['message'])
  return response

def _write_log(log_type, error_type, message, *args, **kwargs):
  if log_type == 'info':
    log.info("{0}: {1}".format(error_type, message))
  elif log_type == 'error':
    log.error("{0}: {1}".format(error_type, message))
  else:
    log.debug("{0}: {1}".format(error_type, message))



def start():
  # db.eviction.update_data()
  data = get_data()
  for item in data:
    item['state'] = state_map[item.get('state')]
    item['area_id'] = item['state'] + "-" + item['sity'].upper()
    areas = get_area_id(to_utf8(item['area_id']))
    for area in areas:
      print area, item.get('eviction_rate')
      if area:
        insert_dict = dict(
          Stat_Name='Eviction_Rate',
          User_Id=0,
          Stat_Type='EVICTION',
          Base_Type='AREA',
          Base_Id=area.get('result'),
          Description='eviction_rate',
          Stat_Value=item.get('eviction_rate'),
          Stat_Date=datetime.datetime.now().strftime("%Y-%m-%d"),
          Data_Source='https://evictionlab.org/'
        )
        insert_db('statistics', insert_dict)
      else:
        message = 'Not Found: {0}'.format(item['area_id'])
        _write_log('info', '', message)



if __name__ == '__main__':
  start()