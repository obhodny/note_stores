#!/usr/bin/python
#-*- coding':' utf-8 -*-
#
# Author':' Vladimir <obhodny@gmail.com>
# Created':' 2018-01-23
#

"""prepare_zipcode_list.py
prepare and create new .csv file
source: ZipcodesToSupport.scv

"""

import os
from lib.storage import db, mysql
import warnings
import logging, logging.handlers
import lib.config as config
import csv
import re


base_path = os.path.dirname(os.path.abspath(__file__))
formatter = logging.Formatter(config.LOG_FORMAT)
logging.root.setLevel(logging.NOTSET)

class MyLogger:
  def __init__(self, file_name):
    self.log_file = '../logs/{file_name}.log'.format(file_name=file_name)
    self.hdlr = logging.FileHandler(self.log_file, 'a')
    self.hdlr.setFormatter(formatter)
    self.logger = logging.getLogger(self.log_file)
    self.logger.addHandler(self.hdlr)

logger = MyLogger('prepare_zipcode_list')
log = logger.logger

def _write_log(log_type, error_type, message, *args, **kwargs):
  if log_type == 'info':
    log.info("{0}: {1}".format(error_type, message))
  elif log_type == 'error':
    log.error("{0}: {1}".format(error_type, message))
  else:
    log.debug("{0}: {1}".format(error_type, message))

def _prepare_zip(zip):
  if zip:
    zips = re.compile("[^0-9]").split(zip)
    zip = zips[0]
    if len(zip) < 5:
      for i in range(5 - len(zip)):
        zip = '0' + zip
  return zip

def insert_db(table, data):
  response = dict(error=False, error_type='', message='', id=None)
  with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
      id = db.base_storage.add(data, table=table, debug=False)
      response.update(id=id)
    except mysql.Error as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    except Warning as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    finally:
      pass
  if response['error']:
    _write_log('error', response['error_type'], response['message'])
  return response

def get_csv_data(file_path):
  rows = []
  with open(file_path, 'r') as f:
    reader = csv.reader(f)
    next(reader)
    for row in reader:
      row[1] = _prepare_zip(row[1])
      rows.append(row)
  return rows

def create_csv(file_path, row):

  with open(file_path, 'wb') as f:
    writer = csv.writer(f)
    writer.writerow('Property State,Property Zip,status')
    writer.writerows(row)

def write_csv(file_path, in_data):
  column_head =['Property State', 'Property Zip', 'status']
  for data in in_data:
    file_exist = os.path.isfile(file_path)
    with open(file_path, 'a') as csvfile:
      writer = csv.DictWriter(csvfile, fieldnames=column_head)
      if not file_exist:
        writer.writeheader()
      try:
        writer.writerow(data)
      except csv.Error as e:
        _write_log('error', type(e).__name__, e)


def main():
  csv_file_input = os.path.dirname(os.path.abspath(__file__)) + '/ZipcodesToSupport.csv'
  csv_file_output = os.path.dirname(os.path.abspath(__file__)) + '/ZipcodesToSupport_extend.csv'
  data_from_csv = get_csv_data(csv_file_input)
  insert_data = []
  city_list = []
  for item in data_from_csv:
    print item
    where = "zip = {0} AND state = '{1}'".format(item[1], item[0])
    select = 'city'
    data = db.zipcodes.get(where=where, select=select, as_dict=True)
    if data['city'] in city_list:
      continue
    print data
    select2 = 'zip, city, state'
    where2 = "city = '{0}' AND state = '{1}'".format(data['city'], item[0])
    data2 = db.zipcodes.get_all(where=where2, select=select2, as_dict=True)
    for item2 in data2:
      print item2
      insert_data.append({'Property State': item2['state'],
                           'Property Zip': item2['zip'],
                           'status': ''})
    city_list.append(data['city'])
  write_csv(csv_file_output, insert_data)



if __name__ == '__main__':
  main()

