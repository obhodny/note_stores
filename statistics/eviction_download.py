#!/usr/bin/python
#-*- coding: utf-8 -*-
#
# Author: Vladimir <obhodny@gmail.com>
# Created: 2018-04-26
#

"""eviction_download.py
downloads .csv files from S3 Amazon AWS
details: https://evictionlab.org/

"""

# from lib.storage import db, mysql
from lib.httptools import Http
import os
import xml.etree.ElementTree as ET
import wget
import time
import logging, logging.handlers
import lib.config as config




formatter = logging.Formatter(config.LOG_FORMAT)
logging.root.setLevel(logging.NOTSET)
base_path = os.path.dirname(os.path.abspath(__file__))


class MyLogger:
  def __init__(self, file_name):
    self.log_file = base_path + '/logs/{file_name}.log'.format(file_name=file_name)
    self.hdlr = logging.FileHandler(self.log_file, 'a')
    self.hdlr.setFormatter(formatter)
    self.logger = logging.getLogger(self.log_file)
    self.logger.addHandler(self.hdlr)

logger = MyLogger('eviction')
log = logger.logger
http = Http('', api_key=None)
path = '{0}/{1}/{2}'. format(base_path, 'downloads', 'eviction-lab-data.xml')
s3_path = 'http://eviction-lab-data-downloads.s3.amazonaws.com/'
output_path = '{0}/{1}'. format(base_path, 'downloads')

def call_key_list():
  res = http.get(s3_path)
  with open(path, 'a') as f:
    f.write(res)

def get_key_list():
  key_list = []
  tree = ET.parse(path)
  root = tree.getroot()
  for child in root:
    for item in child.findall('Key'):
      key_list.append(item.text)
  return key_list


def get_files(key_list, output_directory):
  for key in key_list:
    time.sleep(10)
    url = '{0}{1}'.format(s3_path, key)
    try:
      name = key.replace('/', '_')
      out_filename = '{0}/{1}'.format(output_directory, name)
      filename = wget.download(url, out_filename)
      message = '{0} [{1}] download success'. format(filename, key)
      print message
      log.info("{0}: {1}".format('', message))
    except BaseException as e:
      log.error("{0}: {1}".format('', key))
      log.error("{0}: {1}".format(type(e).__name__, str(e)))
      print 'Error', key
      continue

def main():
  key_list = get_key_list()
  get_files(key_list, output_path)


if __name__ == '__main__':
  main()
