#!/usr/bin/python
#-*- coding: utf-8 -*-
#
# Author: Vladimir <obhodny@gmail.com>
# Created: 2018-01-23
#

"""eviction_csv-db.py
inserting data from .csv files to DB
source: ../downloads/*.csv
inserting to nt.evection

"""

import os
from lib.storage import db
import csv

base_path = os.path.dirname(os.path.abspath(__file__))

def get_files():
  list_file_path = []
  upload_location = '{0}/{1}/'.format(base_path, 'downloads')
  for file in os.listdir(upload_location):
    filename, file_extension = os.path.splitext(file)
    name = filename.split('_')
    name = name[1] if len(name)>1 else None
    if file_extension == '.csv' and name == 'cities':
      list_file_path.append(''.join((upload_location, filename, file_extension)))
  return list_file_path

def create_table():
  fields=['GEOID','year','name','parent-location','population','poverty-rate','pct-renter-occupied','median-gross-rent',
          'median-household-income','median-property-value','rent-burden','pct-white','pct-af-am',
          'pct-hispanic','pct-am-ind','pct-asian','pct-nh-pi','pct-multiple','pct-other',
          'renter-occupied-households','eviction-filings','evictions','eviction-rate','eviction-filing-rate',
          'imputed','subbed']
  text = ' VARCHAR (255),\n '.join(fields) + ' VARCHAR (255),'
  text = text.replace('-', '_')
  command="""CREATE TABLE IF NOT EXISTS eviction (
  id INT NOT NULL AUTO_INCREMENT,
  {0}
  PRIMARY KEY (id)
) ENGINE=MyISAM CHARSET=utf8;
   """.format(text)
  db.base_storage.execute(command)
  return fields


def insert_to_table(file_path):
  with open(file_path, mode='r') as csvfile:
    reader = csv.reader(csvfile)
    column_head = next(reader)
    count = 0
    for row in reader:
      count += 1
      data = {"%s" % column_head[x].replace('-', '_'): row[x] for x in range(0, len(column_head))}
      print count
      db.base_storage.add(data, table='eviction')




def main():
  create_table()
  # list_file_path = get_files()
  # for file_path in list_file_path:
  #   print file_path
  #   insert_to_table(file_path)
  insert_to_table(base_path + "/downloads/US_cities.csv")


if __name__ == '__main__':
  main()