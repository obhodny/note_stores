#!/usr/bin/python
#-*- coding: utf-8 -*-
#
# Author: Vladimir <obhodny@gmail.com>
# Created: 2018-04-26
#

import csv
import os
from lxml import etree
from lib.storage import db, mysql
import warnings
import lib.config as config
import logging, logging.handlers
from lib.httptools import Http
import json
import datetime
import time
import random
import re

"""greatschools.py
Get data from https://api.greatschools.org by zip-code list.
Insert to nt.schools
Prepare and insert to nt.statistics

"""

formatter = logging.Formatter(config.LOG_FORMAT)
logging.root.setLevel(logging.NOTSET)

class MyLogger:
  def __init__(self, file_name):
    self.log_file = 'logs/{file_name}.log'.format(file_name=file_name)
    self.hdlr = logging.FileHandler(self.log_file, 'a')
    self.hdlr.setFormatter(formatter)
    self.logger = logging.getLogger(self.log_file)
    self.logger.addHandler(self.hdlr)

logger = MyLogger('greatschools')
log = logger.logger

http = Http('https://api.greatschools.org', api_key=config.greatschools_api_key)


def _prepare_zip(zip):
  if zip:
    zips = re.compile("[^0-9]").split(zip)
    zip = zips[0]
    if len(zip) < 5:
      for i in range(5 - len(zip)):
        zip = '0' + zip
  return zip


def get_nearby_schools(state, zip):
  """

  :param: None
  :return: data: dict of response api
  """
  xml_txt = http.get_api('schools/nearby', state, zip=zip, id=None)
  data = []
  parser = etree.XMLParser(recover=True)
  schools = etree.fromstring(xml_txt, parser=parser)
  for school in schools:
    data2 = dict()
    for item in school:
      text = item.text.replace('\n', '') if item.text else ''
      data2.update({item.tag: text})
    data.append(data2)
  return data

def get_school_test_scores(state, school_id):
  """

   :param id: school gsID
   :return: data: dict of response api (school's test)
   """
  xml_txt = http.get_api('school/tests', state, zip=None, id=school_id)
  data = dict()
  parser = etree.XMLParser(recover=True)
  test_results = etree.fromstring(xml_txt, parser=parser)
  for rank in test_results.findall('rank'):
    data2 = dict()
    for item in rank:
      text = item.text.replace('\n', '') if item.text else ''
      data2.update({item.tag: text})
    data.update(rank=data2)
  for test in test_results.findall('test'):
    data2 = dict()
    for item in test:
      text = item.text.replace('\n', '') if item.text else ''
      data2.update({item.tag: text})
    test_list = []
    for testResult in test.findall('testResult'):
      data3 = dict()
      for item in testResult:
        text = item.text.replace('\n', '') if item.text else ''
        data3.update({item.tag: text})
      test_list.append(data3)
      data2.update(testResult=test_list)
    data.update(test=data2)
  return data

def insert_db(table, data):
  response = dict(error=False, error_type='', message='', id=None)
  with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
      id = db.base_storage.add(data, table=table, debug=False)
      response.update(id=id)
    except mysql.Error as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    except Warning as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    finally:
      pass
  if response['error']:
    _write_log('error', response['error_type'], response['message'])
  return response

def _write_log(log_type, error_type, message, *args, **kwargs):
  if log_type == 'info':
    log.info("{0}: {1}".format(error_type, message))
  elif log_type == 'error':
    log.error("{0}: {1}".format(error_type, message))
  else:
    log.debug("{0}: {1}".format(error_type, message))


def get_csv_data(file_path):
  line = None
  res = None
  with open(file_path, 'r') as f:
    reader = csv.reader(f)
    next(reader)
    for row in reader:
      if row[2] != 'completed':
        line = reader.line_num
        res = tuple(row)
        break
  with open(file_path, 'r') as f:
    reader = csv.reader(f)
    title = next(reader)
    rows = []
    for row in reader:
      if reader.line_num == line:
        row[2] = 'completed'
      rows.append(row)
  with open(file_path, 'w') as f:
    writer = csv.writer(f)
    writer.writerow(title)
    writer.writerows(rows)
  return res


def main():
  data = dict()
  school_ids = []
  csv_file_name = os.path.dirname(os.path.abspath(__file__)) + '/ZipcodesToSupport.csv'
  state, zip, status = get_csv_data(csv_file_name)
  _write_log('info', '', 'Getting schools by {0}-{1}'.format(state, zip))
  schools = get_nearby_schools(state, _prepare_zip(zip))
  for school in schools:
    data.update(school)
    data.update(zip=zip)
    school_id = school.get('gsId')

    # get school test scores by id
    if school_id:
      _write_log('info', '', 'Getting school test scores by schoolID {0}'.format(school_id))
      res = get_school_test_scores(state, school_id)
      # rank
      rank = res.get('rank')
      data.update(rank_name=rank.get('name'),
                  rank_scale=rank.get('scale'),
                  rank_year=rank.get('year'),
                  rank_score=rank.get('score'),
                  description=rank.get('description')
                  )
      # test
      test = res.get('test')
      data.update(test_name=test.get('name'),
                  test_level_code=test.get('levelCode'),
                  test_id=test.get('id')
                  )
      # test result
      test_result = test.get('testResult')
      if test_result:
        tests = []
        for test_res in test_result:
          data2 = dict(test_result_name=test_res.get('subjectName'),
                       test_result_score=test_res.get('score'),
                       year=test_res.get('year')
          )
          tests.append(data2)
        data.update(test_results=json.dumps(tests))

    # insert to 'schools'
    response = insert_db('schools', data)
    _write_log('info', '', 'Inserted to nt.schools (id: {0})'.format(response['id']))
    if not response['error']:
      school_ids.append(response['id'])

  # insert to 'statistics'
  school_stats = db.schools.get_by_ids(school_ids)
  for item in school_stats:
    Base_Id = '{0}-{1}-{2}'.format(item.get('zip'), item.get('city'), item.get('state'))
    insert_dict = dict(
      Stat_Name='Height_School',
      User_Id=0,
      Stat_Type='SCHOOL',
      Base_Type='AREA',
      Base_Id=Base_Id,
      Description='greatschools',
      Stat_Value=item.get('gs_rating'),
      Stat_Date=datetime.datetime.now().strftime("%Y-%m-%d"),
      Data_Source='https://greatschools.org'
    )
    insert_db('statistics', insert_dict)
    _write_log('info', '', 'Inserted to nt.statistics (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))


if __name__ == '__main__':
  sleep = random.randint(1, 600)
  message = 'Cron job {0} (pause: {1} sec)'.format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"), sleep)
  _write_log('info', '', message)
  time.sleep(sleep)
  message = 'Start script {0}'.format(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
  _write_log('info', '', message)
  main()

