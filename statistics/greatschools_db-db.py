#!/usr/bin/python
#-*- coding':' utf-8 -*-
#
# Author':' Vladimir <obhodny@gmail.com>
# Created':' 2018-05-09
#

"""greatschools_db-db.py
Updating nt.statistics
add new statistic "E_SCHOOL", "M_SCHOOL" or "SCHOOL"
get from nt.schools, prepare and insert to nt.statistic

"""

import os
from lib.storage import db, mysql
import warnings
import logging, logging.handlers
import lib.config as config
import datetime
from lib.utils import to_utf8
import re

###

schools_db = db.schools2

###

base_path = os.path.dirname(os.path.abspath(__file__))
formatter = logging.Formatter(config.LOG_FORMAT)
logging.root.setLevel(logging.NOTSET)

class MyLogger:
  def __init__(self, file_name):
    self.log_file = '../logs/{file_name}.log'.format(file_name=file_name)
    self.hdlr = logging.FileHandler(self.log_file, 'a')
    self.hdlr.setFormatter(formatter)
    self.logger = logging.getLogger(self.log_file)
    self.logger.addHandler(self.hdlr)

logger = MyLogger('greatschools_to_db')
log = logger.logger


def _prepare_zip(zip):
  if zip:
    zips = re.compile("[^0-9]").split(zip)
    zip = zips[0]
    if len(zip) < 5:
      for i in range(5 - len(zip)):
        zip = '0' + zip
  return zip

def insert_db(table, data):
  response = dict(error=False, error_type='', message='', id=None)
  with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
      id = db.base_storage.add(data, table=table, debug=False)
      response.update(id=id)
    except mysql.Error as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    except Warning as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    finally:
      pass
  if response['error']:
    _write_log('error', response['error_type'], response['message'])
  return response

def insert_if_not_exist(table, data, *args, **kwargs):
  response = dict(error=False, error_type='', message='', id=None)
  with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
      exist = db.base_storage.get('Base_Id = "{}" AND Stat_Type = "{}"'.format(kwargs['Base_Id'], kwargs['Stat_Type']),
                                  table=table)
      if exist:
        pass
      else:
        id = db.base_storage.add(data, table=table, debug=False)
        response.update(id=id)
    except mysql.Error as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    except Warning as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
    finally:
      pass
  if response['error']:
    _write_log('error', response['error_type'], response['message'])
  return response

def _write_log(log_type, error_type, message, *args, **kwargs):
  if log_type == 'info':
    log.info("{0}: {1}".format(error_type, message))
  elif log_type == 'error':
    log.error("{0}: {1}".format(error_type, message))
  else:
    log.debug("{0}: {1}".format(error_type, message))

def insert_elementary_schools():
  school_stats = schools_db.get_elementary()
  for item in school_stats:
    print item
    Base_Id = '{0}-{1}-{2}'.format(_prepare_zip(item.get('zip')), item.get('city'), item.get('state'))
    insert_dict = dict(
      Stat_Name='Elementary_School',
      User_Id=0,
      Stat_Type='E_SCHOOL',
      Base_Type='AREA',
      Base_Id=Base_Id,
      Description='greatschools',
      Stat_Value=item.get('gs_rating'),
      Stat_Date=datetime.datetime.now().strftime("%Y-%m-%d"),
      Data_Source='https://greatschools.org'
    )
    insert_db('statistics', insert_dict)
    _write_log('info', '','Inserted to nt.statistics (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))


def insert_middle_schools():
  school_stats = schools_db.get_middle()
  for item in school_stats:
    print item
    Base_Id = '{0}-{1}-{2}'.format(_prepare_zip(item.get('zip')), item.get('city'), item.get('state'))
    insert_dict = dict(
      Stat_Name='Middle_School',
      User_Id=0,
      Stat_Type='M_SCHOOL',
      Base_Type='AREA',
      Base_Id=Base_Id,
      Description='greatschools',
      Stat_Value=item.get('gs_rating'),
      Stat_Date=datetime.datetime.now().strftime("%Y-%m-%d"),
      Data_Source='https://greatschools.org'
    )
    insert_db('statistics', insert_dict)
    _write_log('info', '','Inserted to nt.statistics (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))

def insert_high_schools():
  school_stats = schools_db.get_high()
  inserted = 0
  skipped = 0
  for item in school_stats:
    print item
    Base_Id = '{0}-{1}-{2}'.format(_prepare_zip(item.get('zip')), item.get('city'), item.get('state'))
    insert_dict = dict(
      Stat_Name='Middle_School',
      User_Id=0,
      Stat_Type='SCHOOL',
      Base_Type='AREA',
      Base_Id=Base_Id,
      Description='greatschools',
      Stat_Value=item.get('gs_rating'),
      Stat_Date=datetime.datetime.now().strftime("%Y-%m-%d"),
      Data_Source='https://greatschools.org'
    )
    res = insert_if_not_exist('statistics', insert_dict, Base_Id=Base_Id, Stat_Type='SCHOOL')
    if res['id'] is not None:
      inserted += 1
      _write_log('info', '', 'Inserted  (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))
    else:
      skipped += 1
      _write_log('info', '', 'Skipped (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))
  print 'Inserted: {0}, Skipped: {1}'.format(inserted, skipped)
  _write_log('info', '', 'Inserted: {0}, Skipped: {1}'.format(inserted, skipped))

def insert_high_schools2():
  school_stats = schools_db.get_high_by_city()
  inserted = 0
  skipped = 0
  for item in school_stats:
    print item
    Base_Id = '{0}-{1}'.format(item.get('state'), item.get('city'))
    insert_dict = dict(
      Stat_Name='Middle_School',
      User_Id=0,
      Stat_Type='SCHOOL',
      Base_Type='AREA',
      Base_Id=Base_Id,
      Description='greatschools by city',
      Stat_Value=item.get('gs_rating'),
      Stat_Date=datetime.datetime.now().strftime("%Y-%m-%d"),
      Data_Source='https://greatschools.org'
    )
    res = insert_if_not_exist('statistics', insert_dict, Base_Id=Base_Id, Stat_Type='SCHOOL')
    if res['id'] is not None:
      inserted += 1
      _write_log('info', '', 'Inserted  (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))
    else:
      skipped += 1
      _write_log('info', '', 'Skipped (Base_Id: {0}, Stat_Value: {1})'.format(Base_Id, item.get('gs_rating')))
  print 'Inserted: {0}, Skipped: {1}'.format(inserted, skipped)
  _write_log('info', '', 'Inserted: {0}, Skipped: {1}'.format(inserted, skipped))


if __name__ == '__main__':
  _write_log('info', '', "\nStart script [{0}]\n".format(datetime.datetime.now()))
  # insert_elementary_schools()
  # insert_middle_schools()
  insert_high_schools() # area = 'zip-city-state'
  # insert_high_schools2() # area = 'state-city'