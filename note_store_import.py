#!/usr/bin/python
#-*- coding: utf-8 -*-
#
# Author: Vladimir <obhodny@gmail.com>
# Created: 2018-01-23
#

import csv
import os
import lib.config as config
from lib.utils import to_utf8
import re
import time
import datetime
import logging, logging.handlers
from lib.storage import db
from lib.storage import db_connect, mysql
import warnings
import lib.utils as utils
import subprocess
import json
import openpyxl



formatter = logging.Formatter(config.LOG_FORMAT)
logging.root.setLevel(logging.NOTSET)
PRIVILRGE_CODE = config.PRIVILRGE_CODES[0]


class MyLogger:
  def __init__(self, file_name):
    self.log_file = config.DIR + file_name + '.logtmp'
    self.hdlr = logging.FileHandler(self.log_file, 'a')
    self.hdlr.setFormatter(formatter)
    self.logger = logging.getLogger(self.log_file)
    self.logger.addHandler(self.hdlr)

def _prepare_error_description(line=None, error_type=None, message=None):
  """

  :param line:
  :param error_type:
  :param message:
  :return: error_description
  """
  line = '[Line: {0}]'.format(line) if line else ''
  error_type = '{0}'.format(error_type) if error_type else ''
  message = '{0}'.format(message) if message else ''
  error_description = '# {0} {1} {2}'.format(error_type, line, message)
  return error_description

def get_upload_location():
  """

  :return: upload_location
  """
  return db.parameters.get_upload_location()

def get_files():
  """

  :return: list_file_path
  """
  list_file_path = []
  upload_location = config.DIR
  #upload_location = get_upload_location()
  for file in os.listdir(upload_location):
    filename, file_extension = os.path.splitext(file)
    if file_extension == '.csv':
      list_file_path.append(''.join((config.DIR, filename, file_extension)))
    if file_extension == '.xls' or file_extension == '.xlsx':
      xls_to_csv(''.join((config.DIR, filename, file_extension)), file_extension)
  return list_file_path

def parse_csv(file_path, user_id, log, user_subscriptions):
  """

  :param file_path:
  :param user_id:
  :param log:
  :param user_subscriptions:
  :return: {
    "status": "statusname",
    "total_count": 10,
    "error_count": 2,
    "errors": {
        "1": "errorname",
        "5": "errorname",
        "x": "errorname"
    }
}
  """
  errors = []
  status = ''
  total_count = 0
  error_count = 0
  uploaded_count = 0
  reason = None
  try:
    with open(file_path, mode='r') as csvfile:
      reader = csv.reader(csvfile)
      column_head = None
      try:
        column_head = next(reader)
      except csv.Error as e:
        _write_log(log, type(e).__name__, e, line=reader.line_num)
      if not column_head:
        return
      column_head = _normalize_list_item(column_head)
      file_header = _prepare_head(column_head)
      store_name = get_note_store(file_header)
      if store_name:
        function_names = get_function(store_name)
        module_name, method_name = function_names[0].split('.') if function_names else (None, None)
        module_name = config.STORES_DIR + module_name
        module = __import__(module_name, fromlist=[''])
        store_obj = module.Store(store_name, user_id)
        error = False
        warning = False
        message = ''
        count_notes = 0
        counter = 1
        counter_notes = 0
        try:
          for i, row in enumerate(reader):
            if ''.join(row).strip():
              if row[0][0] == "#":
                continue
              print 'row: ', row
              row = _normalize_list_item(row)
              print 'normalize_row: ', row
              processed_row = _prepare_csv_row(row, column_head, log)
              if processed_row:
                print 'processed_row: ', processed_row
                counter += 1
                total_count +=1
                count_notes = user_subscriptions['count_notes'] + counter_notes
                if user_subscriptions['privilege_value'] and count_notes >= user_subscriptions['privilege_value']:
                  error = config.ERROR_NOTE_CREATE_OR_UPLOAD
                  message = config.ERROR_NOTE_CREATE_OR_UPLOAD_MESSAGE
                  error_count +=1
                  if reason != 'subscription':
                    errors.append("{0}: {1}".format(counter, str(message)))
                  status = 'Partial Upload'
                  if reason != 'subscription':
                    _write_log(log, '', message, line=counter)
                  reason = 'subscription'
                  error_description = _prepare_error_description(line=counter, error_type='', message=message)
                  write_csv(file_path, row, column_head, log, error_description=error_description)
                  print "[{0}] {1}".format(datetime.datetime.now(), error_description)
                  continue
                else:
                  error = False
                  warning = False
                  message = ''
                  response = insert_to_db(processed_row, row, log,
                                     function_names=function_names, store_name=store_name,
                                     store_obj=store_obj, user_id=user_id,
                                     line=counter,
                                     # line=reader.line_num,
                                     column_head=column_head, file_path=file_path,
                                     error=error, warning=warning,
                                     message=message
                                     )
                  error = response['error']
                  warning = response['warning']
                  reason = response['reason']
                  message = response['message']
                  print "[{0}] response: {1}".format(datetime.datetime.now(), str(response))
                  print "-------------------------------------------------------------------"
                  if not error and not warning:
                    counter_notes += 1
                    uploaded_count += 1
                  if error or warning:
                    error_count += 1
                    errors.append("{0}: {1}".format(counter, message))
                    status = 'Partial Upload'
                    _write_log(log, '', message, line=counter)
                    error_description = _prepare_error_description(line=counter, error_type='', message=message)
                    write_csv(file_path, row, column_head, log, error_description=error_description)
              else:
                error = True
            else:
              continue
          if len(errors) > 0:
            print 'total_count:', total_count
            print 'error_count', error_count
            print 'uploaded_count', uploaded_count
            if error_count == total_count:
              status = 'Upload Failed'
              _write_log2(file_path, '', 'Upload Failed')
            else:
              _write_log2(file_path, '', 'Partial Upload')
          else:
            status = 'Loaded Successfull'
            _write_log2(file_path, '', 'Loaded Successfull')
        except csv.Error as e:
          status = 'Upload Failed'
          reason = 'internal error'
          errors.append("{0}".format('Internal Error'))
          _write_log2(file_path, type(e).__name__, 'Upload Failed')
          print e
          return {'totalCount':total_count, 'errorCount':error_count, 'status':status, 'errors':errors, 'uploadedCount':uploaded_count, 'reason':reason}
      else:
        status = 'Upload Failed'
        reason = 'bad header'
        errors.append("{0}".format(config.ERROR_BAD_HEADER_MESSAGE))
        _write_log(log, '', config.ERROR_BAD_HEADER_MESSAGE)
        _write_log2(file_path, '', 'Upload Failed')
        # os.rename(file_path, file_path.replace(".inprocess", "") + ".bad")
  except Exception as e:
    status = 'Upload Failed'
    reason = 'internal error'
    errors.append("{0}".format('Internal Error'))
    _write_log2(file_path, type(e).__name__, 'Internal Error')
    print e
    return {'totalCount':total_count, 'errorCount':error_count, 'status':status, 'errors':errors, 'uploadedCount':uploaded_count, 'reason':reason}
  return {'totalCount':total_count, 'errorCount':error_count, 'status':status, 'errors':errors, 'uploadedCount':uploaded_count, 'reason':reason}

def insert_to_db(row, origin_row, log, *args, **kwargs):
  """

  :param row:
  :param origin_row:
  :param log:
  :param args:
  :param kwargs:
  :return: response dict
  """

  if kwargs.get('error'):
    return dict(error=True, warning=False, error_type='', message=kwargs.get('message'))
  store_obj = kwargs.get('store_obj')
  property_id = None
  note_id = None
  insert_data_1 = None
  insert_data_2 = None
  response = dict(error=False, warning=False, error_type='', message='', reason='')


  if not response['error']:
    try:
      insert_data_1 = store_obj.insert_property(row)
    except Exception as e:
      response.update(error=True, error_type=type(e).__name__, message=e,
                      table_name='property', key_name='Property_id', id=property_id)

  if not response['error']:
    with warnings.catch_warnings():
      warnings.filterwarnings('error')
      try:
        print 'property: ', insert_data_1
        property_id = db.property_db.add_or_update(insert_data_1,
                                                      insert_data_1.get('user_Id'),
                                                      insert_data_1.get('Street_Address'),
                                                      insert_data_1.get('City'),
                                                      insert_data_1.get('State'),
                                                      insert_data_1.get('Zip'),
                                                      debug=False
                                                      )

      except mysql.Error as e:
        print '--property------------------------------------------'
        error_msg = db.application_codes.get_application_code(e)
        if error_msg:
          mysql_error_message = error_msg
        else:
          mysql_error_message = utils.prepare_mysql_error(e)
          mysql_error_message = utils.replace_field_name(mysql_error_message)
        print mysql_error_message
        response.update(error=True, error_type=type(e).__name__, message=mysql_error_message,
                        table_name='property', key_name='Property_id', id=property_id)
      except Warning as e:
        print '--property------------------------------------------'
        error_msg = db.application_codes.get_application_code(e)
        if error_msg:
          mysql_error_message = error_msg
        else:
          mysql_error_message = utils.prepare_mysql_error(e)
          mysql_error_message = utils.replace_field_name(mysql_error_message)
        print mysql_error_message
        response.update(error=True, warning=True, error_type=str(e), message=mysql_error_message,
                        table_name='property', key_name='Property_id', id=property_id)

  if not response['error']:
    with warnings.catch_warnings():
      warnings.filterwarnings('error')
      try:
        if property_id:
          property_areas_exist = db.property_areas.get_by_property_id(property_id)
          if not property_areas_exist:
            data1, data2 = _prepare_property_areas_data(property_id, insert_data_1)
            area_exist1 = db.area.get_by_property_id_type(data1['Area_Id'], data1['Area_type']) # CITY
            area_exist2 = db.area.get_by_property_id_type(data2['Area_Id'], data2['Area_type']) # ZIP-CITY-STATE

            if not area_exist1:
              a_city, a_state = '', ''
              if insert_data_1.get('City'):
                a_city = insert_data_1['City'].upper()
              if insert_data_1.get('State'):
                a_state = insert_data_1['State'].upper()
              area_data1=dict(
                Area_Id=data1['Area_Id'],
                Area_type=data1['Area_type'],
                Area_name="City-{0}".format(data1['Area_Id']),
                Area_Description="City BOAZ of AL".format(a_city, a_state)
              )
              db.area.add_with_ignore(area_data1)

            if not area_exist2:
              area_description = "{0}-{1}".format(data2['Area_type'], data2['Area_Id'])
              area_data2 = dict(
                Area_Id=data2['Area_Id'],
                Area_type=data2['Area_type'],
                Area_name=area_description,
                Area_Description=area_description
              )
              db.area.add_with_ignore(area_data2)

            db.property_areas.add_with_ignore(data1)
            db.property_areas.add_with_ignore(data2)

      except mysql.Error as e:
        print '---property_areas-----------------------------------------'
        error_msg = db.application_codes.get_application_code(e)
        if error_msg:
          mysql_error_message = error_msg
        else:
          mysql_error_message = utils.prepare_mysql_error(e)
          mysql_error_message = utils.replace_field_name(mysql_error_message)
        print mysql_error_message
        response.update(error=True, error_type=type(e).__name__, message=mysql_error_message,
                        table_name='property_areas', key_name='Property_id', id=property_id)
      except Warning as e:
        print '---property_areas-----------------------------------------'
        error_msg = db.application_codes.get_application_code(e)
        if error_msg:
          mysql_error_message = error_msg
        else:
          mysql_error_message = utils.prepare_mysql_error(e)
          mysql_error_message = utils.replace_field_name(mysql_error_message)
        print mysql_error_message
        response.update(error=True, warning=True, error_type=str(e), message=mysql_error_message,
                        table_name='property_areas', key_name='Property_id', id=property_id)

  if not response['error']:
    file_name = os.path.basename(kwargs.get('file_path'))
    file_name = file_name.replace(".inprocess", "") if file_name else None
    try:
      insert_data_2 = store_obj.insert_note(row, property_id, file_name=file_name)
    except Exception as e:
      response.update(error=True, error_type=type(e).__name__, message=e,
                      table_name='note', key_name='Property_id', id=property_id)
  if not response['error'] and property_id:
    with warnings.catch_warnings():
      warnings.filterwarnings('error')
      try:
        # note_id = db.note.add_with_ignore(insert_data_2)
        print 'note: ', insert_data_2
        note_id = db.note.add_or_update(insert_data_2,
                                        insert_data_1.get('user_Id'),
                                        insert_data_1.get('Street_Address'),
                                        insert_data_1.get('City'),
                                        insert_data_1.get('State'),
                                        insert_data_1.get('Zip')
                                        )
      except mysql.Error as e:
        print '---note-----------------------------------------'
        error_msg = db.application_codes.get_application_code(e)
        if error_msg:
          mysql_error_message = error_msg
        else:
          mysql_error_message = utils.prepare_mysql_error(e)
          mysql_error_message = utils.replace_field_name(mysql_error_message)
        print mysql_error_message
        response.update(error=True, error_type=type(e).__name__, message=mysql_error_message,
                        table_name='note', key_name='Note_id', id=note_id)
      except Warning as e:
        print '---note-----------------------------------------'
        error_msg = db.application_codes.get_application_code(e)
        if error_msg:
          mysql_error_message = error_msg
        else:
          mysql_error_message = utils.prepare_mysql_error(e)
          mysql_error_message = utils.replace_field_name(mysql_error_message)
        print mysql_error_message
        response.update(error=True, warning=True, error_type=str(e), message=mysql_error_message,
                        table_name='note', key_name='Note_id', id=note_id)
  if response['error']:
    response.update(reason='value error')
  return response

def get_note_store(file_headers):
  """

  :param file_headers:
  :return: Store Name
  """
  res = db.note_store.get(where='Import_file_headers = "{0}"'.format(file_headers),
                           select='Store_Name', as_dict=True)
  response = res['Store_Name'] if res else None
  return response

def get_sql_text(store_name):
  """

  :param store_name:
  :return:
  """
  res = db.interface_table_mapping.get_all(where='Store_Name = "{0}"'.format(store_name),
                           select='SQL_text, Loading_order', order_by='Loading_order', as_dict=True)
  return [i['SQL_text'] for i in res]

def get_function(store_name):
  """

  :param store_name:
  :return:
  """
  res = db.interface_table_mapping.get_all(where='Store_Name = "{0}"'.format(store_name),
                           select='function_name, Loading_order', order_by='Loading_order', as_dict=True)
  return [i['function_name'] for i in res]

def write_csv(file, data, column_head, log, error_description=None):
  """

  :param file:
  :param data:
  :param column_head:
  :param log:
  :param error_description:
  :return: None
  """
  file = file.replace(".inprocess", "")
  file_exist = os.path.isfile(file + '.bad')
  data2 = {}
  try:
    data2 = {"%s" % column_head[x]: to_utf8(data[x]) for x in range(0, len(column_head))}
  except IndexError as e:
    _write_log(log, 'Error', 'Internal error', log_type='error')
    print e

  except KeyError as e:
    _write_log(log, 'Error', 'Internal error', log_type='error')
    print e

  with open(file + '.bad', 'a') as csvfile:
      writer = csv.DictWriter(csvfile, fieldnames=column_head)
      if not file_exist:
        writer.writeheader()
      try:
        writer.writerow({column_head[0]: error_description})
      except csv.Error as e:
        _write_log(log, type(e).__name__, e)
        print e

  with open(file+'.bad', 'a') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=column_head)
    try:
      writer.writerow(data2)
    except csv.Error as e:
      _write_log(log, type(e).__name__, e)
      print e

def get_message(data):
  """

  :param data:
  :return: Message text
  """
  where = 'function_code = "{0}"'.format(data)
  message = db.application_codes.getall(select='Message', where=where, as_dict=True)
  if message:
    return message[0].get('Message')

def insert_to_integrity(table_name, key_name, key_value, violation_code ):
  """

  :param table_name:
  :param key_name:
  :param key_value:
  :param violation_code:
  :return: None
  """
  data = dict(table_name=table_name ,
  key_name=key_name ,
  key_value=key_value ,
  function_name='note_store_import',
  violation_code=violation_code)
  try:
    db.data_integrity.add(data)
  except mysql.OperationalError as e:
    print e

def _write_log__old(log, error_type, original_message, *args, **kwargs):
  """

  :param log:
  :param error_type:
  :param original_message:
  :param args:
  :param kwargs:
  :return: None
  """
  message = get_message(error_type)
  line = kwargs.get('line') if kwargs.get('line') else ''
  log_type = kwargs.get('log_type') if kwargs.get('log_type') else None
  if log_type == 'info':
    if message:
      log.info("{0}".format(message))
    else:
      log.info("{0} {1}".format(error_type, original_message))
  else:
    if message:
      log.error("{0} {1}".format(line, message))
    else:
      log.error("{0} {1} {2}".format(line, error_type, original_message))

def _write_log(log, error_type, original_message, *args, **kwargs):
  """

  :param log:
  :param error_type:
  :param original_message:
  :param args:
  :param kwargs:
  :return: None
  """
  line = kwargs.get('line') if kwargs.get('line') else ''
  message = "{0} {1} {2}".format(line, error_type, original_message)
  log.error(message.strip())

def _write_log2(log, error_type, original_message, *args, **kwargs):
  print 'log:', log
  file_name = log.replace(".inprocess", ".logtmp")
  f1 = open(file_name, 'r')
  lines = f1.readlines()
  data = [original_message+"\n"]
  data.extend(lines)
  f1.close()
  f2 = open(file_name, 'w')
  f2.writelines("%s" % i for i in data)
  f2.close()

def _prepare_query(query, store_name, user_id):
  """

  :param query:
  :param store_name:
  :param user_id:
  :return: sql_text_list
  """
  query = query.replace("$STORE_NAME", store_name)
  query = query.replace("$USER", user_id)
  sql_text_list = query.split(';')
  sql_text_list.pop(len(sql_text_list) - 1)
  return sql_text_list

def _prepare_head(column_head):
  """

  :param column_head:
  :return: Header Items
  """
  file_header = ''.join(column_head).replace(' ', '').upper()
  reg = re.compile('[^A-Z0-9 ]')
  file_header = reg.sub('', file_header)
  return file_header

def _normalize_list_item(lst):
  """

  :param lst:
  :return: list items
  """
  res = []
  for column_name in lst:
    for charachter in config.REPLACE_CHARACTER_LIST:
      res.append(column_name.replace(charachter, ''))
  return res

def _prep_val(val):
  # print '_prep_val', repr(val)
  try:
    val = val.strip().replace('\t', '').replace('\n', '').replace('\r', '')
    # val = re.sub('[\s+]', '', val)
  except Exception as e:
    print str(e)
    return val
  else:
    return val


def _prepare_csv_row(row, column_names, log):
  """

  :param row:
  :param column_names:
  :param log:
  :return: csv row
  """
  keys = []
  for key in column_names:
    key = re.sub(r'([^A-Z0-9a-z])', '', key).replace(' ', '').upper()
    keys.append(key)
  try:
    data = {keys[x]: _prep_val(utils.to_utf8(row[x])) for x in range(0, len(row))}
    return data
  except IndexError as e:
    _write_log(log, 'Error', 'Internal error of import', log_type='error')
    print e

  except KeyError as e:
    _write_log(log, 'Error', 'Internal error of import', log_type='error')
    print e

def _prepare_file(file_path):
  """

  :param file_path:
  :return: file path
  """
  input_file = file_path + ".inprocess"
  command = "cat '{input_file}'|tr '\r' '\n'|grep -v '^ *$' > {output_file}".format(
    input_file=file_path,
    output_file=input_file
  )
  subprocess.call(command, shell=True)
  os.remove(file_path)
  return file_path + ".inprocess"

def _prepare_file2(file_path):
  """

  :param file_path:
  :return: file path
  """
  file2 = file_path + ".inprocess"
  with open(file_path, 'r') as f1:
    with open(file2, 'w') as f2:
      for line in f1:
        f2.write(line.replace('\r', '\n'))
  os.rename(file_path, file_path + ".bak")
  return file2

def xls_to_csv(file_path, ex):
  try:
    wb = openpyxl.load_workbook(file_path)
    sh = wb.get_active_sheet()
  except Exception as e:
    print e
  else:
    new_file = file_path.replace(ex, '') + '.csv'
    with open(new_file, 'wb') as f:
      c = csv.writer(f)
      for r in sh.rows:
        c.writerow([cell.value for cell in r])
  os.rename(file_path, file_path + ".bak")


def _write_result_file(filename, data):
  print "[{0}] creating: {1}".format(datetime.datetime.now(), filename)
  try:
    with open(filename, 'w') as outfile:
      json.dump(data, outfile)
  except Exception as e:
    print e

def subscription(user_id):
  """

  :param user_id:
  :return: user privileges
  """
  response = dict(error=False, message='')
  user_subscription = db.user_subscriptions.get_by_user_id(user_id)
  if not user_subscription:
    user_subscription = db.user_subscriptions.get_default(user_id)
  if user_subscription:
    subscription_name = user_subscription[0]['subscription_name']
  else:
    subscription_name = 'DEFAULT'
  subscription_privileges = db.subscription_privileges.get_by_name(subscription_name, privilege_code=PRIVILRGE_CODE)
  if subscription_privileges:
      response.update(privilege_value=subscription_privileges[0]['privilege_value'])
      if subscription_privileges[0]['privilege_value']:
        count_notes = db.note.get_count_user_notes(user_id)
        response.update(count_notes=count_notes)
      else:
        response.update(count_notes=-1)
  else:
    response.update(error=config.ERROR_NOTE_CREATE_OR_UPLOAD, message=config.ERROR_NOTE_CREATE_OR_UPLOAD_MESSAGE)
  return response

def _prepare_property_areas_data(property_id, data):
  """

  :param property_id:
  :param data:
  :return: tuple. property types 'CITY', 'ZIP-CITY-STATE'
  """
  area_types = ['CITY', 'ZIP-CITY-STATE']
  area_id_template1 = "{0}-{1}"
  area_id_template2 = "{0}-{1}-{2}"
  if data.get('City') and data.get('State') and data.get('Zip'):
    try:
      area_id1 = area_id_template1.format(data['State'].upper(), data['City'].upper())
      area_id2 = area_id_template2.format(data['Zip'].upper(), data['City'].upper(), data['State'].upper())
      return dict(Area_Id=area_id1, Area_type=area_types[0], Property_id=property_id), \
             dict(Area_Id=area_id2, Area_type=area_types[1], Property_id=property_id)
    except Exception as error:
      print error


def main():
  while 1:
    list_file_path = get_files()
    if list_file_path:
      for file_path in list_file_path:
        file_name = os.path.basename(file_path)
        print "[{0}] got file: {1}".format(datetime.datetime.now(), file_name)
        logger = MyLogger(file_name)
        log = logger.logger
        # file_path = _prepare_file(file_path)
        file_path = _prepare_file2(file_path)
        file_name_list = file_name.split('_')
        if len(file_name_list) > 1:
          if file_name_list[0] and file_name_list[0].isdigit():
            user_id = file_name_list[0]
            user_subscriptions = subscription(user_id)
            if user_subscriptions['error']:
              os.rename(file_path, file_path.replace(".inprocess", "") + ".bad")
              _write_log(log, user_subscriptions['error'], "%s" % user_subscriptions['message'])
              os.rename(file_path.replace(".inprocess", "") + '.logtmp', file_path.replace(".inprocess", "") + ".log")
              continue
            else:
              res = parse_csv(file_path, user_id, log, user_subscriptions)
              print "[{0}] parse_csv.response: {1}".format(datetime.datetime.now(), str(res))
              _write_result_file(file_path.replace(".inprocess", "") + '.json', res)
              print "parse_csv Done\n"
              try:
                if os.path.isfile(file_path):
                  os.rename(file_path, file_path.replace(".inprocess", "") + ".loaded")
                os.rename(file_path.replace(".inprocess", "") + '.logtmp', file_path.replace(".inprocess", "") + ".log")
              except (OSError, IOError) as e:
                print e
          else:
            os.rename(file_path, file_path.replace(".inprocess", "") + ".bad")
            _write_log(log, 'File Error', "File name is bad: %s" % file_path.replace(".inprocess", ""))
            os.rename(file_path.replace(".inprocess", "") + '.logtmp', file_path.replace(".inprocess", "") + ".log")
            continue
        else:
          os.rename(file_path, file_path.replace(".inprocess", "") + ".bad")
          _write_log(log, 'File Error', "File name is bad: %s" % file_path.replace(".inprocess", ""))
          os.rename(file_path.replace(".inprocess", "") + '.logtmp', file_path.replace(".inprocess", "") + ".log")
          continue

    time.sleep(config.SLEEP)


if __name__ == '__main__':
  main()

