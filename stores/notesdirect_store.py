#!/usr/bin/python
#-*- coding: utf-8 -*-

"""notesdirect_store.py

0 :  PROPERTYID
1 :  SERVICERLOANID
2 :  SERVICERNAME
3 :  ASSETTYPE
4 :  PROPERTYTYPE
5 :  PROPERTYADDRESS1
6 :  PROPERTYADDRESS2
7 :  PROPERTYCITY
8 :  PROPERTYSTATE
9 :  PROPERTYZIP
10 :  PROPERTYCOUNTY
11 :  PROPERTYAPNTAXID
12 :  ORIGINATIONDATE
13 :  MATURITYDATE
14 :  FIRSTPAYMENTDUE
15 :  NEXTPAYMENTDUE
16 :  LASTPAYMENTRECEIVED
17 :  LOANSTATUS
18 :  ORIGLOANAMOUNT
19 :  CURRENTUPB
20 :  PIPAYMENT
21 :  ESCROWPAYMENT
22 :  INTERESTRATE
23 :  TERM
24 :  ESTPMTSREMAINING
25 :  LIENPOSITION
26 :  VALUATIONTYPE
27 :  VALUATIONDATE
28 :  VALUATIONAMOUNT
29 :  OFROOMS
30 :  OFBEDROOMS
31 :  OFBATHROOMS
32 :  SQFT
33 :  LOTSIZE
34 :  YEARBUILT
35 :  PRICINGRETAIL
36 :  PRICINGTITANIUM
37 :  PRICINGDIAMOND
38 :  PRICINGPLATINUMBM
39 :  PRICINGPLATINUMRR

"""

from store import BaseStore

class Store(BaseStore):

  def insert_property(self, row):

    """insert_note
        :param list:
          row of csv file
        :return dict:
          data for insert to nt.property

    """
    prop_lot_size = row['LOTSIZE'].upper().replace(' ', '').strip()

    if 'ACRES' in str(prop_lot_size):
      size_type = 'acres'
    elif 'ACRE' in str(prop_lot_size):
      size_type = 'acres'
    elif 'SQFT' in str(prop_lot_size):
      size_type = 'sqft'
    else:
      size_type = None

    prop_lot_size = prop_lot_size.replace('ACRES', '').replace('ACRE', '').replace('SQFT', '')

    print 'size_type, prop_lot_size [1]', size_type, prop_lot_size

    if not self.is_float(prop_lot_size):
      prop_lot_size = self.get_digit(prop_lot_size)

    if self.is_float(prop_lot_size):
      prop_lot_size = float(prop_lot_size)
      if not size_type and prop_lot_size <= 99:
        size_type = 'acres'
      if size_type and size_type == 'acres':
        prop_lot_size = prop_lot_size * 43560 # Acre to Sq ft

    print 'size_type, prop_lot_size [2]', size_type, prop_lot_size

    on_of_prop_unit, property_type = self._prepare_property_type(row['PROPERTYTYPE'])


    insert_data = dict(
      APN                 = row['PROPERTYAPNTAXID'],
      BUILT_YEAR          = self.nullif(row['YEARBUILT']),
      City                = row['PROPERTYCITY'],
      NO_OF_BATHROOMS     = self.nullif(row['OFBATHROOMS']),
      NO_OF_BEDROOMS      = self.nullif(row['OFBEDROOMS']),
      PROP_LOT_SIZE       = prop_lot_size if self.is_float(prop_lot_size) else None,
      Property_Type       = property_type,
      Building_size_sf    = self.nullif(row['SQFT']),
      State               = row['PROPERTYSTATE'],
      Store_Name          = self.store_name,
      Store_PROPERTY_ID   = row['PROPERTYID'],
      Street_Address      = row['PROPERTYADDRESS1'] + ' ' + row['PROPERTYADDRESS2'],
      Zip                 = row['PROPERTYZIP'],
      user_Id             = self.user_id,
      NO_OF_PROP_UNIT     = on_of_prop_unit
    )
    return insert_data

  def insert_note(self, row, property_id, *args, **kwargs):

    """insert_note
        :param list:
          row of csv file
        :return dict:
          data for insert to nt.note

    """

    Note_Type = row['ASSETTYPE'][0:1]

    Origination_Date = self._date_format(row['ORIGINATIONDATE'])
    if not Origination_Date and Note_Type != 'R':
      raise ValueError('Origination Date value error')

    Maturity_Date = self._date_format(row['MATURITYDATE'])

    print 'insert_note.Origination_Date',  Origination_Date
    print 'insert_note.Maturity_Date origin', Maturity_Date

    if not Maturity_Date or (Maturity_Date <= Origination_Date):
      Maturity_Date = self._calculate_maturite_date(row)
      print 'insert_note._calculate_maturite_date.Maturity_Date ', Maturity_Date
      row['MATURITYDATE'] = Maturity_Date

    if not Maturity_Date and Note_Type != 'R':
      raise ValueError('Maturity Date value error')

    print 'Maturity_Date2', Maturity_Date

    if self.is_int(row['TERM']):
      Term_Months = row['TERM']
    else:
      Term_Months = self._diff_month(row['MATURITYDATE'], row['ORIGINATIONDATE'])
      if not Term_Months and Note_Type != 'R':
        raise ValueError('Maturity date must be greater than the loan origination date')


    if row['LASTPAYMENTRECEIVED']:
      last_payment_recieved_date = self._date_format(row['LASTPAYMENTRECEIVED'])
      if not last_payment_recieved_date and Note_Type != 'R':
        raise ValueError('last payment recieved date value error')
    else:
      last_payment_recieved_date = self.nullif(row['LASTPAYMENTRECEIVED'])

    insert_data = dict(
      ESTIMATED_MARKET_VALUE     = self.nullif(self._money_format(row['VALUATIONAMOUNT'])),
      user_id                    = self.user_id,
      Interest_rate_Initial      = self._percent_convert(self._percent_format(row['INTERESTRATE'])),
      loan_type                  = 'CUST_FIXED',
      Note_Position              = row['LIENPOSITION'],
      NOTE_PRICE                 = self._money_format(row['PRICINGRETAIL']),
      Note_Type                  = Note_Type,
      ORIGINAL_LOAN_BAL          = row['ORIGLOANAMOUNT'],
      Origination_Date           = Origination_Date,
      PDI_PAYMENT                = row['PIPAYMENT'],
      Property_id                = property_id,
      REMAINING_NO_OF_PAYMENT    = self.nullif(row['ESTPMTSREMAINING']) if self.is_int(row['ESTPMTSREMAINING']) else None,
      Store_Name                 = self.store_name,
      Store_Note_id              = row['PROPERTYID'],
      Term_Months                = Term_Months,
      UNPAID_BALANCE             = row['CURRENTUPB'],
      last_payment_recieved_date = last_payment_recieved_date,
      TX_ID                      = kwargs.get('file_name'),
      maturity_Date              = Maturity_Date
    )
    return insert_data


  #########################################
  #########################################

  def _prepare_property_type(self, value=None):

    """_get_property_type
    :param string value:
      value
    :return string value:

    """
    value = value.replace(' ', '').strip().upper()
    print 'input property_type: ', value

    on_of_prop_unit = None
    if value == 'DUPLEX':
      on_of_prop_unit = 2
    elif value == 'TRIPLEX':
      on_of_prop_unit = 3
    elif value == 'FOURPLEX':
      on_of_prop_unit = 4

    default_type = 'Unknown'
    input_types = {
      'SINGLEFAMILY': 'SFR',
      'MODULAR': 'SFR',
      'CONDOMINIUM': 'CONDO',
      'MULTIFAMILY': 'MULTI-FAM',
      'TOWNHOUSE': 'TH',
      'MANUFACTUREDDOUBLE-WIDE': 'MFG-HOME',
      'MANUFACTUREDSINGLE-WIDE': 'MFG-HOME',

      #new 2019-09-05

      'DUPLEX': 'MULTI-FAM2-4',
      'TRIPLEX':  'MULTI-FAM2-4',
      'FOURPLEX': 'MULTI-FAM2-4',
      'MULTI-FAM': 'MULTI-FAM5+',
    }

    property_type = input_types.get(value) if input_types.get(value) else default_type
    print 'output on_of_prop_unit, property_type: ', on_of_prop_unit, property_type

    return on_of_prop_unit, property_type

    


