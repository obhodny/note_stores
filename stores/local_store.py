#!/usr/bin/python
#-*- coding: utf-8 -*-

"""local_store.py

0 :  PROPERTYTYPE
1 :  STREETADDRESS
2 :  CITY
3 :  STATE
4 :  ZIP
5 :  NOTETYPE
6 :  ORIGINATIONDATE
7 :  ORIGLOANAMOUNT
8 :  INTERESTRATE
9 :  NOTEPOSITION
10 :  NOTEPRICE
11 :  TERMMONTHS
12 :  LOANTYPE
13 :  BORROWERCREDITSCORE
14 :  UNPAIDBALANCE
15 :  BUILTYEAR
16 :  PROPLOTSIZEACRES
17 :  BUILDINGSIZESF
18 :  NOOFBATHROOMS
19 :  NOOFBEDROOMS
20 :  APN
21 :  REMAININGPAYMENTS
22 :  LASTPMTRECEIVEDDATE
23 :  VALUATIONTYPE
24 :  VALUATIONDATE
25 :  VALUATIONAMOUNT

"""

from store import BaseStore

class Store(BaseStore):

  def insert_property(self, row):

    insert_data = dict(
      Property_Type     = row['PROPERTYTYPE'],
      Street_Address    = row['STREETADDRESS'],
      City              = row['CITY'].upper(),
      State             = row['STATE'].upper(),
      Zip               = row['ZIP'],
      Built_Year        = self.nullif(row['BUILTYEAR']),
      Prop_Lot_Size     = self.nullif(row['PROPLOTSIZEACRES'].upper().replace('ACRES', '').replace('ACRE', '').replace(' ', '')),
      Building_size_sf  = self.nullif(row['BUILDINGSIZESF']),
      No_of_bathrooms   = self.nullif(row['NOOFBATHROOMS']),
      No_of_bedrooms    = self.nullif(row['NOOFBEDROOMS']),
      APN               = row['APN'],
      Store_name        = self.store_name,
      Store_Property_id = None,
      user_Id           = self.user_id
    )
    return insert_data

  def insert_note(self, row, property_id, *args, **kwargs):


    Origination_Date = self._date_format(row['ORIGINATIONDATE'])
    if not Origination_Date:
      raise ValueError('Origination Date value error')

    if row['LASTPMTRECEIVEDDATE']:
      last_payment_recieved_date = self._date_format(row['LASTPMTRECEIVEDDATE'])
      if not last_payment_recieved_date:
        raise ValueError('last payment recieved date value error')
    else:
      last_payment_recieved_date = self.nullif(row['LASTPMTRECEIVEDDATE'])

    Interest_rate_Initial = self._percent_convert(self._percent_format(row['INTERESTRATE']))
    if not Interest_rate_Initial or Interest_rate_Initial == 0 or Interest_rate_Initial == 0.0:
      raise ValueError('Interest Rate value error. Value must be greater than zero')


    insert_data = dict(
      ESTIMATED_MARKET_VALUE     = self.nullif(self._money_format(row['VALUATIONAMOUNT'])),
      Note_Type                  = row['NOTETYPE'],
      Origination_Date           = Origination_Date,
      REMAINING_NO_OF_PAYMENT    = self.nullif(row['REMAININGPAYMENTS']),
      last_payment_recieved_date = last_payment_recieved_date,
      ORIGINAL_LOAN_BAL          = self._money_format(row['ORIGLOANAMOUNT']),
      Interest_rate_Initial      = Interest_rate_Initial,
      Loan_Type                  = self.nullif(row['LOANTYPE']),
      Note_Position              = self.nullif(row['NOTEPOSITION']),
      Borrower_Credit_Score      = self.nullif(row['BORROWERCREDITSCORE']),
      Note_Price                 = self._money_format(row['NOTEPRICE']),
      Store_Name                 = self.store_name,
      user_id                    = self.user_id,
      Property_id                = property_id,
      term_months                = self.nullif(row['TERMMONTHS']),
      Unpaid_balance             = self.nullif(self._money_format(row['UNPAIDBALANCE'])),
      TX_ID                      = kwargs.get('file_name')
    )
    return insert_data


  #########################################
  #########################################








