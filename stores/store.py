#!/usr/bin/python
#-*- coding: utf-8 -*-


from datetime import datetime
from dateutil import relativedelta
import re
import lib.utils as utils

class BaseStore(object):

  def __init__(self, store_name, user_id):
    self.store_name = store_name
    self.user_id = user_id


  def _prepare_csv_row(self, row):
    data = {"c%.2d" % x: utils.to_utf8(row[x - 1]) for x in range(1, len(row) + 1)}
    return data

  def _prepare_zip(self, zip):
    if zip:
      zips = re.compile("[^0-9]").split(zip)
      zip = zips[0]
      if len(zip) < 5:
        for i in range(5 - len(zip)):
          zip = '0' + zip
    return zip


  def _money_format(self, data):
    return data.replace('$', '').replace(',', '').replace(' ', '')

  def _percent_format(self, data):
    return data.replace('%', '').replace(',', '').replace(' ', '')

  def _percent_convert2(self, data):
    value = float(data)
    if value > 1:
      return value / 100
    else:
      return value

  def _percent_convert(self, data):
    value = float(data)
    return value / 100


  def _date_format(self, date=None):
    print '_date_format input', date
    """"insert_note
        :param date string:
          input formats:
          "%m/%d/%Y"
          "%m/%d/%y"
          "%m.%d.%Y"
          "%m-%d-%Y"
          "%Y/%m/%d"
          "%Y.%m.%d"
          "%Y-%m-%d"
          and time:
          "00:00"
          "0:0"
        :return date string:
          output format:
          "%Y-%m-%d"
         If input format not valid:
        :return input value unchanged

    """
    # with time
    mat3 = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{4})[ ]+(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$', date)
    mat4 = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{4})[ ]+(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$', date)
    mat5 = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{4})[ ]+(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$', date)
    mat6 = re.match('(\d{4})[/.-](\d{1,2})[/.-](\d{1,2})[ ]+(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$', date)
    mat7 = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{1,2})[ ]+(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$', date)
    if mat3 or mat4 or mat5 or mat6 or mat7 is not None:
      lst = date.split(' ')
      date = lst[0]

    # without time
    mat = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{4})$', date)
    mat2 = re.match('(\d{4})[/.-](\d{1,2})[/.-](\d{1,2})$', date)
    mat3 = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{1,2})$', date)
    delimiters = ['/', '.', '-']

    delimiter = '/'
    for l in delimiters:
      if date.find(l) >= 1:
        delimiter = l

    try:
      if mat is not None:
        lst = (map(int, mat.groups()[-1::-1]))
        new_date = ''
        for i in lst:
          new_date = str(i) + delimiter + new_date
        date = datetime.strptime(new_date[:-1], "%m{delimiter}%d{delimiter}%Y".format(delimiter=delimiter)).strftime(
          "%Y-%m-%d")
        print '_date_format output [1]', date
        return date
      elif mat2 is not None:
        lst = (map(int, mat2.groups()[-1::-1]))
        new_date = ''
        for i in lst:
          new_date = str(i) + delimiter + new_date
        date =  datetime.strptime(new_date[:-1], "%Y{delimiter}%m{delimiter}%d".format(delimiter=delimiter)).strftime(
          "%Y-%m-%d")
        print '_date_format output [2]', date
        return date
      elif mat3 is not None:
        lst = (map(int, mat3.groups()[-1::-1]))
        lst[0] = lst[0] if len(str(lst[0])) > 1 else '0%s' % lst[0]
        new_date = ''
        for i in lst:
          new_date = str(i) + delimiter + new_date
        date =  datetime.strptime(new_date[:-1], "%m{delimiter}%d{delimiter}%y".format(delimiter=delimiter)).strftime(
          "%Y-%m-%d")
        print '_date_format output [3]', date
        return date
      else:
        # return
        print '_date_format output [4]', date
        return date
    except ValueError as e:
      return

  def _calculate_maturite_date(self, row):
    origination_date = self._date_format(row['ORIGINATIONDATE'])
    if not origination_date:
      return
    if not row['TERM']:
      return
    try:
      months = int(row['TERM'])
    except Exception:
      return
    date_after_month = datetime.strptime('2013-06-14', "%Y-%m-%d") + relativedelta.relativedelta(months=months)
    return date_after_month.strftime("%Y-%m-%d")



  def _upper(self, data):
    return data.upper()

  def _replace(self, data, **kwargs):
    if kwargs.get('replace_string'):
      data = data.replace(kwargs.get('replace_string'), '').replace(' ', '')
    elif kwargs.get('replace_strings'):
      for replace_string in kwargs.get('replace_strings'):
        data = data.replace(replace_string, '').replace(' ', '')
    return data


  def _replace_and_upper(self, data, **kwargs):
    data = data.upper()
    if kwargs.get('replace_string'):
      data = data.replace(kwargs.get('replace_string'), '').replace(' ', '')
    elif kwargs.get('replace_strings'):
      for replace_string in kwargs.get('replace_strings'):
        data = data.replace(replace_string, '').replace(' ', '')
    return data

  def _diff_month(self, date1, date2):
    date1 = self._date_format(date1)
    date2 = self._date_format(date2)
    # print 'date1', date1
    # print 'date2', date2
    if not date1 or not date2:
      return
    if date1 < date2:
      return
    try:
      date1 = datetime.strptime(date1, "%Y-%m-%d")
      date2 = datetime.strptime(date2, "%Y-%m-%d")
    except ValueError as e:
      print 'store._diff_month: ', e
    else:
      try:
        diff = relativedelta.relativedelta(date1, date2)
        month = 0
        if diff.years:
          month = diff.years * 12
        month = month + diff.months
        return month
      except TypeError as e:
        print 'store._diff_month: ', e


  def _validation_term_months(self, data):
    return data


  def _validation_date(self, date):
    response = dict(error=False, error_type='', message='')
    try:
      mat = re.match('(\d{1,2})[/.-](\d{1,2})[/.-](\d{4})$', date)
      mat2 = re.match('(\d{4})[/.-](\d{1,2})[/.-](\d{1,2})$', date)
      delimiters = ['/', '.', '-']

      delimiter = '/'
      for l in delimiters:
        if date.find(l) >= 1:
          delimiter = l

      if mat is not None:
        lst = (map(int, mat.groups()[-1::-1]))
        new_date = ''
        for i in lst:
          new_date = str(i) + delimiter + new_date
        datetime.strptime(new_date[:-1], "%m{delimiter}%d{delimiter}%Y".format(delimiter=delimiter)).strftime(
          "%Y-%m-%d")
        return response

      elif mat2 is not None:
        lst = (map(int, mat2.groups()[-1::-1]))
        new_date = ''
        for i in lst:
          new_date = str(i) + delimiter + new_date
        datetime.strptime(new_date[:-1], "%Y{delimiter}%m{delimiter}%d".format(delimiter=delimiter)).strftime(
          "%Y-%m-%d")
        return response

      else:
        response.update(error=True, error_type='ValueError',
                        message="time data {date} does not match format '%m{delimiter}%d{delimiter}%Y'".format(
                          date=date, delimiter=delimiter))
        return response

    except ValueError as e:
      response.update(error=True, error_type=type(e).__name__, message=e)
      return response

  def _prepare_query(self, query):
    query = query.replace("$STORE_NAME", self.store_name)
    query = query.replace("$USER", self.user_id)
    sql_text_list = query.split(';')
    sql_text_list.pop(len(sql_text_list) - 1)
    return sql_text_list

  def _is_required(self, row, column_head, required_column):
    response = dict(error=False, error_type='', message='')
    for i in range(len(row)):
      for j in required_column:
        if j == column_head[i] and not row[i]:
          response.update(error=True, error_type='Empty Value', message='Value is required: {0}'.format(column_head[i]))
          break
    return response

  def nullif(self, value):
    value = value.strip()
    return None if not value else value

  def is_float(self, value):
    if value == 'NaN':
      return False
    try:
      float(value)
      return True
    except:
      return False

  def is_int(self, value):
    if value == 'NaN':
      return False
    try:
      int(value)
      return True
    except:
      return False

  def get_digit(self, value):
    lst = re.findall(r"\d+", value)
    if len(lst) == 1:
      return int(lst[0])
    elif len(lst) == 2:
      return float(".".join(lst))
    else:
      return value





